import lineareRegression as lR
import matplotlib.pyplot as plt
import numpy as np
import csv_Parser as CSV

data = CSV.CTA('Cassy/messung_b_1.lab')
#print(data[1,:])

K_s = 0.16293 / 1000 #m/Ohm
sigma_K_s = 8e-05 / 1000 #m/Ohm
A_s = 0.3528 #m
sigma_A_s = 1.4e-03 #m
sigma_R_stat = 0.7048 #Ohm
sigma_R_sys = 5.774 #Ohm

R = data[:, 3] * 1000 #Ohm
U = data[:, 2] #V

sigma_U_stat = 1 / (2**11 * np.sqrt(12)) * np.ones_like(U)
sigma_U_sys = 0.015 * 1 * np.ones_like(U)

s = R * K_s + A_s #m
sigma_s_stat = K_s * sigma_R_stat * np.ones_like(s)
sigma_s_sys = np.sqrt((K_s*sigma_R_sys)**2 + (R*sigma_K_s)**2 + (sigma_A_s)**2)

U_0 = 0
index = 0
for i in range(len(U)):
    if U[i] > U_0:
        U_0 = U[i]
        index = i
s_0 = s[i]

sigma_x_stat = sigma_s_stat / s
sigma_x_sys = sigma_s_sys / s
sigma_y_stat = sigma_U_stat / U
sigma_y_sys = sigma_U_sys / U

x = s
y = U
#x = np.log(x)
#y = np.log(y)
xerr = sigma_x_sys
yerr = sigma_y_sys
xerr_sys = sigma_x_sys
yerr_sys = sigma_y_sys

a, b, cov, chiq, p = lR.linreg_xy_cov(x, y, ex = xerr,  ey = yerr)
a_p, b_p, _, _, _ = lR.linreg_xy_cov(np.array(x) - np.array(xerr_sys), np.array(y) + np.array(yerr_sys), ex = xerr,  ey = yerr)
a_m, b_m, _, _, _ = lR.linreg_xy_cov(np.array(x) + np.array(xerr_sys), np.array(y) - np.array(yerr_sys), ex = xerr,  ey = yerr)

ea_sys = np.abs(a_p - a_m)/2.
eb_sys = np.abs(b_p - b_m)/2.

plt.errorbar(x, y, xerr = xerr, yerr = yerr, label = 'Daten', linestyle = '-', marker = '.')
X = np.linspace(np.min(x) - 0.1*(np.max(x) - np.min(x)),np.max(x) + 0.1*(np.max(x) - np.min(x)), 10)

plt.plot(X, a*X+b, label = 'LinReg')
plt.plot(X, a_p*X+b_p, label = 'LinReg +', color = 'black')
plt.plot(X, a_m*X+b_m, label = 'LinReg -', color = 'black')


plt.xlabel(r'Strecke $ln(\frac{r}{r_0})$')
plt.ylabel(r'Spannung $ln(\frac{U}{U_0})$')
plt.title(r'Lineare Regression für a)')

plt.grid()
plt.legend()
plt.tight_layout()

print(f'chiq/dof: {chiq/(len(x) - 2)}\tp-value: {p}\nparams:\n\ta: {a}, b: {b}\n\tea: {np.sqrt(cov[0,0])} (stat.), eb: {np.sqrt(cov[1,1])} (stat.)\n\tea: {ea_sys} (syst.), eb: {eb_sys} (syst.)\ncov: {cov}')
plt.show()
