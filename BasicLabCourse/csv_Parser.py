import numpy as np

# Wandelt Cassy-Daten in Numpy-Array um:
def CTA(fname):
    file = open(fname, 'r')
    data_lines = file.read().splitlines()
    data = np.zeros((len(data_lines), len(data_lines[0].split('\t'))))
    for i in range(len(data_lines)):
        current_line = data_lines[i].split('\t')
        for j in range(len(current_line)-1):
            current_line[j] = current_line[j].replace(' ', '0')
            current_line[j] = current_line[j].replace(',', '.')
            current_line[j] = current_line[j].replace('NAN', '0')
            #print(current_line)
            data[i, j] = current_line[j]

    return data

# Wandelt Oszilloskop-Daten in Numpy-Array um:
def OTA(fname, skiplines = 0):
    file = open(fname, 'r')
    lines = file.readlines()

    for i in range(skiplines, len(lines)):
        line = lines[i]
        line = line.replace(',', '', 3)
        line = line.replace('\n', '')
        line = line.replace('NAN', '0')
        lines[i] = line

    data = np.zeros((len(lines), 2))

    for i in range(len(lines)):
        data[i, 0], data[i, 1], _ = lines[i].split(',')

    return data

# Wandelt Numpy-Arrays in Latex um:
    # col ist ein Array mit den ausgewählten Spaltenindizes z.B.: [1,3,4]
def ATL(data, col):
    text = ''
    for i in range(data.shape[0]):
        for j in col:
            text += '{}'.format(data[i, j])
            if col.index(j) != len(col) - 1:
                text += ' & '

        if i != data.shape[0] - 1:
            text += ' \\\ \n'

    return text
