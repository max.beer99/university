import numpy as np
import matplotlib.pyplot as plt
import csv_Parser as CSV

filename = 'Cassy/messung_a_2.lab'
ix = 3
iy = [2]
hist = 0
smallstyle = 1
title = '' #r'Phasengang $20\Omega$'
label = 'Daten'
x_label = r'Strecke $\frac{s}{m}$'
y_label = r'Spannung $\frac{U}{V}$'

style = '.' if smallstyle else 'o'

data = CSV.CTA(filename)

cut = 0
for i in range(1, data.shape[0]):
    if data[i-1, 0] > data[i, 0]:
        cut = i
if cut != 0:
    data_set_1 = np.zeros((cut, data.shape[1]))
    data_set_2 = np.zeros((data.shape[0] - cut, data.shape[1]))
    data_set_1 = data[:cut, :]
    data_set_2 = data[cut:, :]

print(data[1, :])

def stat_ana(X):
    print(f'µ: {np.mean(X)}\tsigma: {np.std(X)}')

if len(iy) > 1:
    fig, ax = plt.subplots(nrows = len(iy), sharex = True)
    j = 0
    for i in iy:
        if cut == 0:
            stat_ana(data[:, i])
            ax[j].scatter(data[:, ix], data[:, i], label = i, marker = style)
        else:
            stat_ana(data_set_1[:, i])
            stat_ana(data_set_2[:, i])
            ax[j].scatter(data_set_1[:, ix], data_set_1[:, i], label = f'{i}, set_1', marker = style)
            ax[j].scatter(data_set_2[:, ix], data_set_2[:, i], label = f'{i}, set_2', marker = style)

        ax[j].legend()
        ax[j].grid()
        j+=1

else:
    stat_ana(data[:, iy[0]])
    if hist:
        plt.hist(data[:, iy[0]], label = label)
        plt.ylabel('N')
        plt.xlabel(y_label)
    else:
        if cut == 0:
            plt.scatter(data[:, ix], data[:, iy[0]], label = label, marker = style)
        else:
            plt.scatter(data_set_1[:, ix], data_set_1[:, iy[0]], label = f'{label}, set_1', marker = style)
            plt.scatter(data_set_2[:, ix], data_set_2[:, iy[0]], label = f'{label}, set_2', marker = style)

        plt.xlabel(x_label)
        plt.ylabel(y_label)



    plt.title(title)
    plt.legend()
    plt.grid()

plt.tight_layout()
plt.show()
