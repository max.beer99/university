import numpy as np
import scipy.odr as sodr
import scipy.stats as scs
from sigfig import round as sigfigRound
import matplotlib

def parseCSV(filename):
    file = open(filename, 'r')
    raw = file.readlines()

    print(f'{filename} read')

    data = np.zeros((len(raw), len(raw[0].split('\t'))))
    for i in range(data.shape[0]):
        split = raw[i].replace('\n', '').split('\t')
        for j in range(data.shape[1]):
            data[i, j] = split[j]

    return data.astype('float')

def pancake(a):
    # good lord please forgive me
    return np.concatenate([entry.flatten() for entry in a]).flatten()

def round(value, sigma, scientific = False, tex = True):
    if tex:
        return sigfigRound(float(value), float(sigma), cutoff = 29, notation = 'sci' if scientific else 'std', separation = ' \pm ')
    else:
        return sigfigRound(float(value), float(sigma), cutoff = 29, notation = 'sci' if scientific else 'std')

def round_syst(value, sigma_stat, sigma_syst, tex = False):
    vr, sstr = round(value, sigma_stat, tex = True).split(' \pm ')
    ssyr = np.round(sigma_syst, len(sstr) - 2) if sigma_stat < 1 else np.round(sigma_syst, len(sstr) - 1)
    return f"{vr}"+" \\pm "+f"{sstr}"+" \\pm "+f"{ssyr}"

def round_array(value, sigma):
    vr = []
    for i in range(len(value)):
        vr.append('$'+round(value[i], sigma[i], tex = True)+'$')

    return np.array(vr)

def simpleGauss(B, x):
    # B[0] = my ; B[1] = sigma
    return B[2]/np.sqrt(2*np.pi*B[1]**2) * np.exp(-(x-B[0])**2/(2*B[1]**2))

def offsetGauss(B, x):
    # B[0] = my ; B[1] = sigma
    return B[3] + B[2]/np.sqrt(2*np.pi*B[1]**2) * np.exp(-(x-B[0])**2/(2*B[1]**2))

def constant(B, x):
    return B[0]*np.ones_like(x)

def linear(B, x):
    return B[0]*x + B[1]

def exponential(B, x):
    return B[2] + B[1] * np.exp(B[0] * x)

def sine(B, x):
    return B[3] + np.abs(B[2]) * np.sin(np.abs(B[0]) * x + B[1])

def fit_arbitrary(xdata, ydata, xerr, yerr, beta0, model):
    data = sodr.RealData(xdata, ydata, sx = xerr, sy = yerr)
    odr = sodr.ODR(data, sodr.Model(model), beta0 = beta0)
    return odr.run()

def sine_fit(xdata, ydata, xerr, yerr, w0 = 1, phase0 = 0, A0 = 1, offset0 = 0):
    model = sodr.Model(sine)
    beta0 = [w0, phase0, A0, offset0]
    data = sodr.RealData(xdata, ydata, sx = xerr, sy = yerr)
    odr = sodr.ODR(data, model, beta0 = beta0)
    return odr.run()

def exponential_fit(xdata, ydata, xerr, yerr, Lambda0 = 1, A0 = 1, offset0 = 0):
    model = sodr.Model(exponential)
    beta0 = [Lambda0, A0, offset0]
    data = sodr.RealData(xdata, ydata, sx = xerr, sy = yerr)
    odr = sodr.ODR(data, model, beta0 = beta0)
    return odr.run()

def constant_fit(xdata, ydata, xerr, yerr, c0 = 0):
    model = sodr.Model(constant)
    beta0 = [c0]
    data = sodr.RealData(xdata, ydata, sx = xerr, sy = yerr)
    odr = sodr.ODR(data, model, beta0 = beta0)
    return odr.run()

def linear_fit(xdata, ydata, xerr, yerr, m0 = 1, b0 = 0):
    model = sodr.Model(linear)
    beta0 = [m0, b0]
    data = sodr.RealData(xdata, ydata, sx = xerr, sy = yerr)
    odr = sodr.ODR(data, model, beta0 = beta0)
    return odr.run()

def gaussian_fit(xdata, ydata, xerr, yerr, my0 = 0, sigma0 = 1, A0 = 1, offset0 = 0, simple = False):
    model = sodr.Model(simpleGauss)
    beta0 = [my0, sigma0, A0]
    if not simple:
        model = sodr.Model(offsetGauss)
        beta0 = [my0, sigma0, A0, offset0]
    data = sodr.RealData(xdata, ydata, sx = xerr, sy = yerr)
    odr = sodr.ODR(data, model, beta0 = beta0)
    return odr.run()

def calc_derivative(xdata, beta, model = simpleGauss, minimal_delta_x = 1e-20):
    delta_x = xdata[1:] - xdata[:-1]
    delta_x_min = np.min(delta_x)/10
    delta_x_min = np.max([delta_x_min, minimal_delta_x])
    return (model(beta, xdata + delta_x_min) - model(beta, xdata - delta_x_min)) / (2*delta_x_min)

def calc_2d_error(xdata, ydata, xerr, yerr, beta, model = simpleGauss):
    return np.sqrt(yerr**2 + (xerr * calc_derivative(xdata, beta, model))**2)

def weighted_mean(x, sigma):
    x = np.array(x)
    sigma = np.array(sigma)
    return (np.sum(x/sigma**2)/np.sum(1/sigma**2), np.sqrt(1/np.sum(1/sigma**2)))

def chi_sq_dof_p(xdata, ydata, xerr, yerr, beta, model = simpleGauss):
    chiq = np.sum((ydata - model(beta, xdata))**2/calc_2d_error(xdata, ydata, xerr, yerr, beta, model)**2)
    dof = xdata.shape[0] - beta.shape[0]
    chiq_dof = chiq/dof
    p_value = 1 - scs.chi2.cdf(chiq, dof)
    return (chiq_dof, p_value)

def A0_from_max(max, sigma):
    return max * np.sqrt(2*np.pi*sigma**2)

def latex_rc_params(fontsize=11, figsize=(0, 0), dpi = 0):
    matplotlib.rcdefaults()
    matplotlib.rcParams.update({
    'figure.max_open_warning': False,
    #'figure.constrained_layout.use': True,
    'text.usetex': True,
    'font.family': 'serif',
    'font.serif': 'cmr',
    'font.size': fontsize,
    'font.weight': 'medium',
    'legend.fontsize': fontsize,
    'axes.labelsize': fontsize,
    'axes.labelweight': 'medium',
    'figure.titlesize': fontsize,
    'figure.titleweight': 'medium',
    'axes.titlesize': fontsize,
    'axes.titleweight': 'medium',
    'xtick.labelsize': fontsize,
    'ytick.labelsize':fontsize,
    'lines.marker': '.',
    'lines.markersize': 1
    })
    if dpi != 0:
        matplotlib.rcParams.update({'figure.dpi': dpi})
    if figsize != (0, 0):
        matplotlib.rcParams.update({'figure.figsize': figsize})

def get_ticks(min, max):
    #max = max + 0.1 * (max - min)
    #min = min - 0.1 * (max - min)

    max_mag = 10 ** np.floor(np.log10(max))
    max_mag = 10 ** np.floor(np.log10(-1 * max)) if max < 0 else max_mag
    root = np.ceil(max / max_mag) * max_mag
    temp_root = root

    ticks_base = [1, 2, 5]
    ticks_minor_number = [5, 4, 5]
    base_iter = 0
    tick_dist = 1
    magnitude = 10 ** np.floor(np.log10((max - min)) - 1)
    tick_dist = magnitude
    tick_dist_minor = tick_dist / ticks_minor_number[base_iter]
    if root % (x := tick_dist) != 0:
        temp_root = root + tick_dist - (root % tick_dist)
    ticks_major = np.arange(temp_root, min, -1 * tick_dist)
    ticks_major = np.append(ticks_major, 0) if min == 0 else ticks_major
    ticks_major = ticks_major[np.where(ticks_major <= max)]
    ticks_major = ticks_major[np.where(ticks_major >= min)]
    while len(ticks_major) > 5:
        if root % (x := ticks_base[base_iter] * magnitude) != 0:
            temp_root = root + x - (root % x)
        ticks_major = np.arange(temp_root, min, -1 * x)
        ticks_major = np.append(ticks_major, 0) if min == 0 else ticks_major
        ticks_major = ticks_major[np.where(ticks_major <= max)]
        ticks_major = ticks_major[np.where(ticks_major >= min)]

        tick_dist = ticks_base[base_iter] * magnitude
        tick_dist_minor = tick_dist / ticks_minor_number[base_iter]
        base_iter = base_iter + 1 if (base_iter + 1) < len(ticks_base) else 0
        magnitude = magnitude * 10 if base_iter == 0 else magnitude

    if root % tick_dist != 0:
        temp_root = root + tick_dist - (root % tick_dist)
    ticks_major = np.arange(temp_root + 3 * tick_dist, min - 3 * tick_dist, -1 * tick_dist)
    ticks_major = np.append(ticks_major, 0) if min == 0 else ticks_major
    ticks_major = ticks_major[np.where(ticks_major <= max)]
    ticks_major = ticks_major[np.where(ticks_major >= min)]

    ticks_minor = np.arange(temp_root + 3 * tick_dist_minor, min - 3 * tick_dist_minor, -1 * tick_dist_minor)
    ticks_minor = np.append(ticks_minor, 0) if min == 0 else ticks_minor
    ticks_minor = ticks_minor[np.where(ticks_minor <= max)]
    ticks_minor = ticks_minor[np.where(ticks_minor >= min)]
    return ticks_major, ticks_minor

def format_grid(x, y, set_scale = True, minor_linestyle = "-", minor_linewidth = 1, setX = True, setY = True):
    assert setX or setY

    txmin, txmax = np.min(x), np.max(x)
    tymin, tymax = np.min(y), np.max(y)

    xmax = txmax + 0.1 * (txmax - txmin)
    xmin = txmin - 0.1 * (txmax - txmin)

    ymax = tymax + 0.1 * (tymax - tymin)
    ymin = tymin - 0.1 * (tymax - tymin)

    if setX:
        if set_scale:
            matplotlib.pyplot.gca().set_xlim(xmin, xmax)

        ticks_major, ticks_minor = get_ticks(xmin, xmax)
        matplotlib.pyplot.gca().xaxis.set_major_locator(matplotlib.ticker.FixedLocator(ticks_major))
        matplotlib.pyplot.gca().xaxis.set_minor_locator(matplotlib.ticker.FixedLocator(ticks_minor))

    if setY:
        if set_scale:
            matplotlib.pyplot.gca().set_ylim(ymin, ymax)

        ticks_major, ticks_minor = get_ticks(ymin, ymax)
        matplotlib.pyplot.gca().yaxis.set_major_locator(matplotlib.ticker.FixedLocator(ticks_major))
        matplotlib.pyplot.gca().yaxis.set_minor_locator(matplotlib.ticker.FixedLocator(ticks_minor))

    matplotlib.pyplot.gca().grid(which = 'major')
    matplotlib.pyplot.gca().grid(which = 'minor', linestyle = minor_linestyle, linewidth = minor_linewidth)

def n_dp(x, n):
    return np.linspace(np.min(x), np.max(x), n)

def latex_tableize(data, titlerow = " titlerow ", swap_xy = False):
    data = data.copy()

    n_rows, n_cols = data.shape
    if swap_xy:
        n_cols, n_rows = n_rows, n_cols
        data = data.T

    cols = r"|X"*n_cols+"|"

    header = r"""
    \smallbreak

    \begin{table}
    \begin{center}
        \caption{}
        \label{}
    	\begin{tabularx}{0.95\textwidth}{""" + cols + r"""}
        \firsthline \rowcolor{hellgrau}""" + "\n\t" + titlerow + r" \\ \hline"+"\n"

    lines = ""
    for row in range(n_rows):
        lines += "\t\t"
        for col in range(n_cols):
            lines += f"{data[row, col]} "
            if col+1 < n_cols:
                lines += "& "
        lines += r"\\ \hline"+"\n"

    foot = "\t"+r"""\end{tabularx}
    \end{center}
    \end{table}

    \smallbreak"""

    return header+lines+foot

def latex_format_table(x, y, titlerow = " titlerow ", xerr="", yerr = np.array([])):
    table = np.empty([y.shape[0], y.shape[1]+1], dtype="<U20")
    for i in range(table.shape[0]):
        if xerr == "":
            table[i, 0] = '$'+str(np.round(x[i]))+'$'
        else:
            table[i, 0] = '$'+round(x[i], xerr[i])+'$'

        for j in range(1, table.shape[1]):
            table[i, j] = '$'+round(y[i, j-1], yerr[i, j-1], tex = True)+'$'

    print(latex_tableize(table, titlerow = titlerow))

def latex_format_table_syst(x, y, titlerow = " titlerow ", xerr="", yerr = np.array([]), yerr_syst = np.array([])):
    table = np.empty([y.shape[0], y.shape[1]+1], dtype="<U20")
    for i in range(table.shape[0]):
        if xerr == "":
            table[i, 0] = np.round(x[i])
        else:
            table[i, 0] = round(x[i], xerr[i])

        for j in range(1, table.shape[1]):
            table[i, j] = round_syst(y[i, j-1], yerr[i, j-1], yerr_syst[i, j-1], tex = True)

    print(latex_tableize(table, titlerow = titlerow))
