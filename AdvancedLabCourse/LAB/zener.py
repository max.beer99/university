import numpy as np
import matplotlib.pyplot as plt
import scipy.odr as sodr
import scipy.stats as scs
from my_methods import *

delta_U = 0.00027#20/(2**12) /np.sqrt(12)
errorscale = 30

zener_1000 = parseCSV('zener_1000')
zener_4700 = parseCSV('zener_4700')
zener_10000 = parseCSV('zener_10000')
x_err = delta_U * np.ones_like(zener_1000[:, 0])
y_err = delta_U * np.ones_like(zener_1000[:, 1])
min = np.min([np.min(zener_1000[:, 1]), np.min(zener_4700[:, 1]), np.min(zener_10000[:, 1])])
max = np.max([np.max(zener_1000[:, 1]), np.max(zener_4700[:, 1]), np.max(zener_10000[:, 1])])

plt.errorbar(zener_1000[:, 0], zener_1000[:, 1], xerr = x_err * errorscale, yerr = y_err * errorscale, label = r'$R = 1 k\Omega$', linestyle = '')
plt.errorbar(zener_4700[:, 0], zener_4700[:, 1], xerr = x_err * errorscale, yerr = y_err * errorscale, label = r'$R = 4.7 k\Omega$', linestyle = '')
plt.errorbar(zener_10000[:, 0], zener_10000[:, 1], xerr = x_err * errorscale, yerr = y_err * errorscale, label = r'$R = 10 k\Omega$', linestyle = '')
plt.ylim(min - 0.1 * (max - min), max + 0.1 * (max - min))
plt.ylabel(r'$U_A [V]$')
plt.xlabel(r'$U_E [V]$')
plt.title(r'Messdaten Zenerdiode$')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/Zener_messung.pdf')
plt.clf()

start = 7 #V
x_1000 = zener_1000[np.abs(zener_1000[:, 0] - start).argmin()::, 0]
x_4700 = zener_4700[np.abs(zener_4700[:, 0] - start).argmin()::, 0]
x_10000 = zener_10000[np.abs(zener_10000[:, 0] - start).argmin()::, 0]
y_1000 = zener_1000[np.abs(zener_1000[:, 0] - start).argmin()::, 1]
y_4700 = zener_4700[np.abs(zener_4700[:, 0] - start).argmin()::, 1]
y_10000 = zener_10000[np.abs(zener_10000[:, 0] - start).argmin()::, 1]

sigma_x_1000 = delta_U * np.ones_like(x_1000)
sigma_x_4700 = delta_U * np.ones_like(x_4700)
sigma_x_10000 = delta_U * np.ones_like(x_10000)
sigma_y_1000 = delta_U * np.ones_like(y_1000)
sigma_y_4700 = delta_U * np.ones_like(y_4700)
sigma_y_10000 = delta_U * np.ones_like(y_10000)

print('########## 1k Ohm ##########')
output = linear_fit(x_1000, y_1000, sigma_x_1000, sigma_y_1000)
G = 1 / output.beta[0]
sigma_G = np.sqrt(output.cov_beta[0, 0]) / (output.beta[0]**2)
chiq_dof, p_value = chi_sq_dof_p(x_1000, y_1000, sigma_x_1000, sigma_y_1000, output.beta, linear)
print(output.beta)
print(np.sqrt(np.diagonal(output.cov_beta)))
print(f'\n\t G: {round(G, sigma_G)}')
print(f'\t chi_sq / dof: {chiq_dof}')
print(f'\t p_value: {p_value}\n')


fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x_1000, linear(output.beta, x_1000), label = r'fit $y = mx + b$', color='C1')
ax[0].errorbar(x_1000, y_1000, xerr = sigma_x_1000, yerr = sigma_y_1000, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x_1000, y_1000 - linear(output.beta, x_1000), xerr = sigma_x_1000, yerr = sigma_y_1000, label = 'residuals', linestyle = '', marker = ',')

ax[0].set(ylabel = r'$U_A [V]$', title = r'Fit für Glättungsfaktor bei $R = 1 k\Omega$')
ax[1].set(xlabel = r'$U_E [V]$', ylabel = r'Data - Fit [V]')
ax[0].text(0.015, 0.955, r'$m$'+f': {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}\n'+r'$b$'+f': {round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n'+r'$\frac{\chi^2}{dof}$'+f': {np.around(chiq_dof, 2)}\np: {p_value}' , horizontalalignment='left', verticalalignment='top', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'lower right')
ax[1].legend()
fig.tight_layout()
fig.savefig(f'./output/glaettung_1000_fit.pdf')
plt.clf()

print('########## 4.7k Ohm ##########')
output = linear_fit(x_4700, y_4700, sigma_x_4700, sigma_y_4700)
G = 1 / output.beta[0]
sigma_G = np.sqrt(output.cov_beta[0, 0]) / (output.beta[0]**2)
chiq_dof, p_value = chi_sq_dof_p(x_4700, y_4700, sigma_x_4700, sigma_y_4700, output.beta, linear)
print(output.beta)
print(np.sqrt(np.diagonal(output.cov_beta)))
print(f'\n\t G: {round(G, sigma_G)}')
print(f'\t chi_sq / dof: {chiq_dof}')
print(f'\t p_value: {p_value}\n')


fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x_4700, linear(output.beta, x_4700), label = r'fit $y = mx + b$', color='C1')
ax[0].errorbar(x_4700, y_4700, xerr = sigma_x_4700, yerr = sigma_y_4700, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x_4700, y_4700 - linear(output.beta, x_4700), xerr = sigma_x_4700, yerr = sigma_y_4700, label = 'residuals', linestyle = '', marker = ',')

ax[0].set(ylabel = r'$U_A [V]$', title = r'Fit für Glättungsfaktor bei $R = 4.7 k\Omega$')
ax[1].set(xlabel = r'$U_E [V]$', ylabel = r'Data - Fit [V]')
ax[0].text(0.015, 0.955, r'$m$'+f': {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}\n'+r'$b$'+f': {round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n'+r'$\frac{\chi^2}{dof}$'+f': {np.around(chiq_dof, 2)}\np: {p_value}' , horizontalalignment='left', verticalalignment='top', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'lower right')
ax[1].legend()
fig.tight_layout()
fig.savefig(f'./output/glaettung_4700_fit.pdf')
plt.clf()

print('########## 1k Ohm ##########')
output = linear_fit(x_10000, y_10000, sigma_x_10000, sigma_y_10000)
G = 1 / output.beta[0]
sigma_G = np.sqrt(output.cov_beta[0, 0]) / (output.beta[0]**2)
chiq_dof, p_value = chi_sq_dof_p(x_10000, y_10000, sigma_x_10000, sigma_y_10000, output.beta, linear)
print(output.beta)
print(np.sqrt(np.diagonal(output.cov_beta)))
print(f'\n\t G: {round(G, sigma_G)}')
print(f'\t chi_sq / dof: {chiq_dof}')
print(f'\t p_value: {p_value}\n')

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x_10000, linear(output.beta, x_10000), label = r'fit $y = mx + b$', color='C1')
ax[0].errorbar(x_10000, y_10000, xerr = sigma_x_10000, yerr = sigma_y_10000, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x_10000, y_10000 - linear(output.beta, x_10000), xerr = sigma_x_10000, yerr = sigma_y_10000, label = 'residuals', linestyle = '', marker = ',')

ax[0].set(ylabel = r'$U_A [V]$', title = r'Fit für Glättungsfaktor bei $R = 10 k\Omega$')
ax[1].set(xlabel = r'$U_E [V]$', ylabel = r'Data - Fit [V]')
ax[0].text(0.015, 0.955, r'$m$'+f': {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}\n'+r'$b$'+f': {round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n'+r'$\frac{\chi^2}{dof}$'+f': {np.around(chiq_dof, 2)}\np: {np.around(p_value, 15)}' , horizontalalignment='left', verticalalignment='top', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'lower right')
ax[1].legend()
fig.tight_layout()
fig.savefig(f'./output/glaettung_10000_fit.pdf')
plt.clf()
