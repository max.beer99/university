import numpy as np
import matplotlib.pyplot as plt
import scipy.odr as sodr
import scipy.stats as scs
from my_methods import *

delta_U = 0.00027#20/(2**12) /np.sqrt(12)
errorscale = 100

R_350k = 352.4e03
sigma_R_350k = 0.1 / np.sqrt(12)
R_4700 = 4680
sigma_R_4700 = 10 / np.sqrt(12)

print('\n\n########## Gleichrichter ##########')
gleichrichter = parseCSV('gleichrichter_3')
t = gleichrichter[:, 0]
U_a = gleichrichter[:, 1]
U_b = gleichrichter[:, 2]

delta_t = (t[1] - t[0])/np.sqrt(12)

t = t[::10]
U_a = U_a[::10]
U_b = U_b[::10]

sigma_t = delta_t * np.ones_like(t)
sigma_U_a = delta_U * np.ones_like(U_a)
sigma_U_b = delta_U * np.ones_like(U_b)

plt.errorbar(t, U_a, xerr = sigma_t, yerr = sigma_U_a * errorscale, label = 'U_A', linestyle = '')
plt.errorbar(t, U_b, xerr = sigma_t, yerr = sigma_U_b * errorscale, label = 'U_B', linestyle = '')
plt.ylabel(r'$U [V]$')
plt.xlabel(r'$t [ms]$')
plt.title(r'Messdaten Gleichrichter')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/gleichrichter_messung.pdf')
plt.clf()

print('\n\n########## Transistoren ##########')
print('\n########## Eingangskennlinie ##########')
transistor_eingang = parseCSV('transistor_eingang_be_2')
U_b = transistor_eingang[:, 0]
U_a = transistor_eingang[:, 1]

U_a = U_a[::1]
U_b = U_b[::1]

I_e = U_b/R_350k

sigma_U_a = delta_U * np.ones_like(U_a)
sigma_U_b = delta_U * np.ones_like(U_b)

sigma_I_e = sigma_U_b / R_350k

errorscale = 1
plt.errorbar(U_a, I_e*1e06, xerr = sigma_U_a, yerr = sigma_I_e*1e06 * errorscale, label = r'$I_B$', linestyle = '')
plt.ylabel(r'$I [µA]$')
plt.xlabel(r'$U_{BE} [V]$')
plt.title(r'Transistor - Kennlinie: Eingang')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/transistor_eingang_be.pdf')
plt.clf()

print('\n########## Steuerkennlinien ##########')
print('########## Spannungssteuer ##########')
transistor_spannungssteuer = parseCSV('transistor_spannungsteuer_be_2')
U_b = transistor_spannungssteuer[:, 0]
U_a = transistor_spannungssteuer[:, 1]

U_a = U_a[::1]
U_b = U_b[::1]

I_c = U_b/R_4700

sigma_U_a = delta_U * np.ones_like(U_a)
sigma_U_b = delta_U * np.ones_like(U_b)

sigma_I_c = sigma_U_b / R_4700

errorscale = 1
plt.errorbar(U_a, I_c*1e06, xerr = sigma_U_a, yerr = sigma_I_c*1e06 * errorscale, label = r'$I_C$', linestyle = '')
plt.ylabel(r'$I [µA]$')
plt.xlabel(r'$U_{BE} [V]$')
plt.title(r'Transistor - Kennlinie: Spannungssteuer')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/transistor_spannungssteuer_be.pdf')
plt.clf()

print('########## Stromsteuer ##########')
transistor_stromsteuer = parseCSV('transistor_stromsteuer_2')
U_b = transistor_stromsteuer[:, 0]
U_a = transistor_stromsteuer[:, 1]

U_a = U_a[::1]
U_b = U_b[::1]

I_b = U_a/R_350k
I_c = U_b/R_4700

sigma_U_a = delta_U * np.ones_like(U_a)
sigma_U_b = delta_U * np.ones_like(U_b)

sigma_I_b = sigma_U_a / R_350k
sigma_I_c = sigma_U_b / R_4700

errorscale = 1
plt.errorbar(I_b*1e06, I_c*1e06, xerr = sigma_I_b*1e06 * errorscale, yerr = sigma_I_c*1e06 * errorscale, label = r'$I_C$', linestyle = '')
plt.ylabel(r'$I [µA]$')
plt.xlabel(r'$I_B [µA]$')
plt.title(r'Transistor - Kennlinie: Stromsteuer')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/transistor_stromsteuer_be.pdf')
plt.clf()

print('\n########## Ausgangskennlinien ###########')
transistor_ausgang_1000 = parseCSV('transistor_ausgang_1000mV_2')
transistor_ausgang_1250 = parseCSV('transistor_ausgang_1250mV_2')
transistor_ausgang_1500 = parseCSV('transistor_ausgang_1500mV_2')

U_b_1000 = transistor_ausgang_1000[:, 0]
U_a_1000 = transistor_ausgang_1000[:, 1]
U_a_1000 = U_a_1000[::1]
U_b_1000 = U_b_1000[::1]
I_c_1000 = U_b_1000/R_4700
sigma_U_a_1000 = delta_U * np.ones_like(U_a_1000)
sigma_U_b_1000 = delta_U * np.ones_like(U_b_1000)
sigma_I_c_1000 = sigma_U_b_1000 / R_4700

U_b_1250 = transistor_ausgang_1250[:, 0]
U_a_1250 = transistor_ausgang_1250[:, 1]
U_a_1250 = U_a_1250[::1]
U_b_1250 = U_b_1250[::1]
I_c_1250 = U_b_1250/R_4700
sigma_U_a_1250 = delta_U * np.ones_like(U_a_1250)
sigma_U_b_1250 = delta_U * np.ones_like(U_b_1250)
sigma_I_c_1250 = sigma_U_b_1250 / R_4700

U_b_1500 = transistor_ausgang_1500[:, 0]
U_a_1500 = transistor_ausgang_1500[:, 1]
U_a_1500 = U_a_1500[::1]
U_b_1500 = U_b_1500[::1]
I_c_1500 = U_b_1500/R_4700
sigma_U_a_1500 = delta_U * np.ones_like(U_a_1500)
sigma_U_b_1500 = delta_U * np.ones_like(U_b_1500)
sigma_I_c_1500 = sigma_U_b_1500 / R_4700

errorscale = 30
plt.errorbar(U_a_1000, I_c_1000*1e06, xerr = sigma_U_a_1000 * errorscale, yerr = sigma_I_c_1000*1e06 * errorscale, label = r'$U_B = 1000$mV', linestyle = '')
plt.errorbar(U_a_1250, I_c_1250*1e06, xerr = sigma_U_a_1250 * errorscale, yerr = sigma_I_c_1250*1e06 * errorscale, label = r'$U_B = 1250$mV', linestyle = '')
plt.errorbar(U_a_1500, I_c_1500*1e06, xerr = sigma_U_a_1500 * errorscale, yerr = sigma_I_c_1500*1e06 * errorscale, label = r'$U_B = 1500$mV', linestyle = '')
plt.ylabel(r'$I_C [µA]$')
plt.xlabel(r'$U_{CE}$')
plt.title(r'Transistor - Kennlinie: Stromsteuer')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/transistor_ausgang.pdf')
plt.clf()

print('\n\n########## Schmitt-Trigger ##########')
print('\n########### Oszilloskop ###########')
schmitt_ac_lang = parseCSV('schmitt_ac_lang')
t = schmitt_ac_lang[:, 0]
U_a = schmitt_ac_lang[:, 1]
U_b = schmitt_ac_lang[:, 2]

U_a = U_a[np.abs(t - 30).argmin()::1]
U_b = U_b[np.abs(t - 30).argmin()::1]
t = t[np.abs(t - 30).argmin()::1]

sigma_t = (t[1] - t[0])/np.sqrt(12) * np.ones_like(t)
sigma_U_a = delta_U * np.ones_like(U_a)
sigma_U_b = delta_U * np.ones_like(U_b)

errorscale = 30
plt.errorbar(t, U_a, xerr = sigma_t * errorscale, yerr = sigma_U_a * errorscale, label = r'$U_A$')#, linestyle = '')
plt.errorbar(t, U_b, xerr = sigma_t * errorscale, yerr = sigma_U_b * errorscale, label = r'$U_B$')#, linestyle = '')
plt.ylabel(r'$U [V]$')
plt.xlabel(r'$t [ms]$')
plt.title(r'Schmitt-Trigger Oszilloskop')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/schmitt_oszi.pdf')
plt.clf()

print('\n########### Hysterese ###########')
schmitt_hysterese = parseCSV('schmitt_hysterese_3')
U_b = schmitt_hysterese[:, 0]
U_a = schmitt_hysterese[:, 1]

U_a = U_a[::1]
U_b = U_b[::1]

sigma_U_a = delta_U * np.ones_like(U_a)
sigma_U_b = delta_U * np.ones_like(U_b)

errorscale = 30
plt.errorbar(U_a, U_b, xerr = sigma_U_a * errorscale, yerr = sigma_U_b * errorscale, label = r'$U_B$')#, linestyle = '')
plt.ylabel(r'$U_B [V]$')
plt.xlabel(r'$U_A [V]$')
plt.title(r'Schmitt-Trigger Hysterese')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/schmitt_hysterese.pdf')
plt.clf()
