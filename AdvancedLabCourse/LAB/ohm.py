import numpy as np
import matplotlib.pyplot as plt
import scipy.odr as sodr
import scipy.stats as scs
from my_methods import *

delta_U = 0.00027#20/(2**12) /np.sqrt(12)
R_ref = 471.4 # Ohm
sigma_R_ref = 0.1 / np.sqrt(12) # Ohm

ohm_1000 = parseCSV('ohm_1000')
ohm_4700 = parseCSV('ohm_4700')
ohm_10000 = parseCSV('ohm_10000')

U_a_1000 = ohm_1000[:, 1]
U_a_4700 = ohm_4700[:, 1]
U_a_10000 = ohm_10000[:, 1]

U_b_1000 = ohm_1000[:, 0]
U_b_4700 = ohm_4700[:, 0]
U_b_10000 = ohm_10000[:, 0]

sigma_U_a_1000 = delta_U * np.ones_like(U_a_1000)
sigma_U_a_4700 = delta_U * np.ones_like(U_a_4700)
sigma_U_a_10000 = delta_U * np.ones_like(U_a_10000)

sigma_U_b_1000 = delta_U * np.ones_like(U_b_1000)
sigma_U_b_4700 = delta_U * np.ones_like(U_b_4700)
sigma_U_b_10000 = delta_U * np.ones_like(U_b_10000)

I_b_1000 = U_b_1000 / R_ref
I_b_4700 = U_b_4700 / R_ref
I_b_10000 = U_b_10000 / R_ref

sigma_I_b_1000 = sigma_U_b_1000 / R_ref
sigma_I_b_4700 = sigma_U_b_4700 / R_ref
sigma_I_b_10000 = sigma_U_b_10000 / R_ref

errorscale = 30
plt.errorbar(I_b_1000 * 1000, U_a_1000, xerr = sigma_I_b_1000 * 1000 * errorscale, yerr = sigma_U_a_1000 * errorscale, linestyle = '', label = r'$R = 1k\Omega$')
plt.errorbar(I_b_4700 * 1000, U_a_4700, xerr = sigma_I_b_4700 * 1000 * errorscale, yerr = sigma_U_a_4700 * errorscale, linestyle = '', label = r'$R = 4.7k\Omega$')
plt.errorbar(I_b_10000 * 1000, U_a_10000, xerr = sigma_I_b_10000 * 1000 * errorscale, yerr = sigma_U_a_10000 * errorscale, linestyle = '', label = r'$R = 10k\Omega$')
plt.ylabel(r'$U_a [V]$')
plt.xlabel(r'$I_b [mA]$')
plt.title('Messung Ohm\'scher Widerstand')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('./output/ohm_plot.pdf')
plt.clf()

errorscale = 1
print('##### 1k #####')
output_a = linear_fit(I_b_1000 + U_b_1000/R_ref**2 * sigma_R_ref, U_a_1000, xerr = sigma_I_b_1000, yerr = sigma_U_a_1000)
output_b = linear_fit(I_b_1000 - U_b_1000/R_ref**2 * sigma_R_ref, U_a_1000, xerr = sigma_I_b_1000, yerr = sigma_U_a_1000)
sigma_m_syst = np.abs(output_a.beta[0] - output_b.beta[0])/2

output = linear_fit(I_b_1000, U_a_1000, xerr = sigma_I_b_1000, yerr = sigma_U_a_1000)
chiq_dof, p_value = chi_sq_dof_p(I_b_1000, U_a_1000, xerr = sigma_I_b_1000, yerr = sigma_U_a_1000, beta = output.beta, model = linear)
error_2d = calc_2d_error(I_b_1000, U_a_1000, xerr = sigma_I_b_1000, yerr = sigma_U_a_1000, beta = output.beta, model = linear)


fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(I_b_1000 * 1000, linear(output.beta, I_b_1000), label = r'fit $y = m \cdot x + b$', color='C1')
ax[0].errorbar(I_b_1000 * 1000, U_a_1000, xerr = sigma_I_b_1000 * 1000, yerr = sigma_U_a_1000, label='data', linestyle = '', marker = ',')
ax[1].errorbar(I_b_1000 * 1000, U_a_1000 - linear(output.beta, I_b_1000), yerr = error_2d, label = 'residuals', linestyle = '', marker = 'o')
ax[0].set(ylabel = r'$U_a [V]$', title = r'Fit für $R = 1k\Omega$')
ax[1].set(xlabel = r'$I_b [mA]$', ylabel = r'Data - Fit [V]')
ax[0].text(0.985, 0.045, r'$m$'+f': {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}(stat.)'+r' $\pm$ '+f'{np.around(sigma_m_syst, 2)}(syst.)\n'+r'$b$'+f': {round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n'+r'$\frac{\chi^2}{dof}$'+f': {np.round(chiq_dof, 1)}\np: {np.around(p_value, 3)}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[1].set_xlim(-np.max(I_b_1000*1000)*0.02, np.max(I_b_1000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left')
ax[1].legend(loc = 'upper left')
fig.tight_layout()
fig.savefig(f'./output/ohm_1000_fit.pdf')
plt.clf()

print('##### 4.7k #####')
output_a = linear_fit(I_b_4700 + U_b_4700/R_ref**2 * sigma_R_ref, U_a_4700, xerr = sigma_I_b_4700, yerr = sigma_U_a_4700)
output_b = linear_fit(I_b_4700 - U_b_4700/R_ref**2 * sigma_R_ref, U_a_4700, xerr = sigma_I_b_4700, yerr = sigma_U_a_4700)
sigma_m_syst = np.abs(output_a.beta[0] - output_b.beta[0])/2

output = linear_fit(I_b_4700, U_a_4700, xerr = sigma_I_b_4700, yerr = sigma_U_a_4700)
chiq_dof, p_value = chi_sq_dof_p(I_b_4700, U_a_4700, xerr = sigma_I_b_4700, yerr = sigma_U_a_4700, beta = output.beta, model = linear)
error_2d = calc_2d_error(I_b_4700, U_a_4700, xerr = sigma_I_b_4700, yerr = sigma_U_a_4700, beta = output.beta, model = linear)


fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(I_b_4700 * 1000, linear(output.beta, I_b_4700), label = r'fit $y = m \cdot x + b$', color='C1')
ax[0].errorbar(I_b_4700 * 1000, U_a_4700, xerr = sigma_I_b_4700 * 1000, yerr = sigma_U_a_4700, label='data', linestyle = '', marker = ',')
ax[1].errorbar(I_b_4700 * 1000, U_a_4700 - linear(output.beta, I_b_4700), yerr = error_2d, label = 'residuals', linestyle = '', marker = 'o')
ax[0].set(ylabel = r'$U_a [V]$', title = r'Fit für $R = 4.7k\Omega$')
ax[1].set(xlabel = r'$I_b [mA]$', ylabel = r'Data - Fit [V]')
ax[0].text(0.985, 0.045, r'$m$'+f': {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}(stat.)'+r' $\pm$ '+f'{np.around(sigma_m_syst, 1)}(syst.)\n'+r'$b$'+f': {round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n'+r'$\frac{\chi^2}{dof}$'+f': {np.round(chiq_dof, 1)}\np: {np.around(p_value, 3)}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[1].set_xlim(-np.max(I_b_4700*1000)*0.02, np.max(I_b_4700*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left')
ax[1].legend(loc = 'upper left')
fig.tight_layout()
fig.savefig(f'./output/ohm_4700_fit.pdf')
plt.clf()

print('##### 10k #####')
output_a = linear_fit(I_b_10000 + U_b_10000/R_ref**2 * sigma_R_ref, U_a_10000, xerr = sigma_I_b_10000, yerr = sigma_U_a_10000)
output_b = linear_fit(I_b_10000 - U_b_10000/R_ref**2 * sigma_R_ref, U_a_10000, xerr = sigma_I_b_10000, yerr = sigma_U_a_10000)
sigma_m_syst = np.abs(output_a.beta[0] - output_b.beta[0])/2

output = linear_fit(I_b_10000, U_a_10000, xerr = sigma_I_b_10000, yerr = sigma_U_a_10000)
chiq_dof, p_value = chi_sq_dof_p(I_b_10000, U_a_10000, xerr = sigma_I_b_10000, yerr = sigma_U_a_10000, beta = output.beta, model = linear)
error_2d = calc_2d_error(I_b_10000, U_a_10000, xerr = sigma_I_b_10000, yerr = sigma_U_a_10000, beta = output.beta, model = linear)

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(I_b_10000 * 1000, linear(output.beta, I_b_10000), label = r'fit $y = m \cdot x + b$', color='C1')
ax[0].errorbar(I_b_10000 * 1000, U_a_10000, xerr = sigma_I_b_10000 * 1000, yerr = sigma_U_a_10000, label='data', linestyle = '', marker = ',')
ax[1].errorbar(I_b_10000 * 1000, U_a_10000 - linear(output.beta, I_b_10000), yerr = error_2d, label = 'residuals', linestyle = '', marker = 'o')
ax[0].set(ylabel = r'$U_a [V]$', title = r'Fit für $R = 10k\Omega$')
ax[1].set(xlabel = r'$I_b [mA]$', ylabel = r'Data - Fit [V]')
ax[0].text(0.985, 0.045, r'$m$'+f': {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}(stat.)'+r' $\pm$ '+f'{np.around(sigma_m_syst, 1)}(syst.)\n'+r'$b$'+f': {round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n'+r'$\frac{\chi^2}{dof}$'+f': {np.round(chiq_dof, 1)}\np: {np.around(p_value, 3)}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left')
ax[1].legend(loc = 'upper left')
fig.tight_layout()
fig.savefig(f'./output/ohm_10000_fit.pdf')
plt.clf()
