import numpy as np
import matplotlib.pyplot as plt
import scipy.odr as sodr
import scipy.stats as scs
from my_methods import *

zener_1000 = parseCSV('zener_10000')
x = np.array(np.arange(zener_1000.shape[0]))
delta_U = zener_1000[:, 0] - 0.05 * x

plt.scatter(x, delta_U, label = r'$U - x \cdot 0.05 V$')
plt.ylim(np.min(delta_U) - 0.1 * (np.max(delta_U) - np.min(delta_U)), np.max(delta_U) + 0.1 * (np.max(delta_U) - np.min(delta_U)))
plt.ylabel(r'$\Delta U$ [V]')
plt.xlabel(r'Index')
plt.title(r'Alle $\Delta_U$')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/rauschen_gesamt.pdf')
plt.cla()

x = x[70::]
delta_U = delta_U[70::]

mean = np.mean(delta_U)
std = np.std(delta_U)
sigma_mean = std / np.sqrt(mean)

plt.scatter(x, delta_U, label = r'$U - x \cdot 0.05 V$')
plt.hlines([mean, mean - std, mean + std], np.min(x), np.max(x))
plt.ylim(np.min(delta_U) - 0.1 * (np.max(delta_U) - np.min(delta_U)), np.max(delta_U) + 0.1 * (np.max(delta_U) - np.min(delta_U)))
plt.ylabel(r'$\Delta U$ [V]')
plt.xlabel(r'Index')
plt.title(r'Bestimmung $\sigma_U$')
plt.text(0.015, 0.98, r'$\mu$'+f': {round(mean, sigma_mean)}\n'+r'$\sigma_U$'+f': {np.round(std, 5)}' , horizontalalignment='left', verticalalignment='top', transform=plt.gca().transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/rauschen_mean.pdf')
