import numpy as np
import matplotlib.pyplot as plt
import scipy.odr as sodr
import scipy.stats as scs
from my_methods import *
import scipy.optimize

energy_scale = 1.6473 #keV
sigma_energy_scale = 0.0011 #keV
energy_offset = -27 #keV
sigma_energy_offset = 0.5 #keV

sigma_angle = 1/np.sqrt(12) # 2.5/np.sqrt(12) 

theta_50 = 50
theta_60 = 60
theta_80 = 80
theta_105 = 105
theta_135 = 135

sigma_theta_50 = sigma_angle
sigma_theta_60 = sigma_angle
sigma_theta_80 = sigma_angle
sigma_theta_105 = sigma_angle
sigma_theta_135 = sigma_angle

def totalCount(x, sigma_x, beta, cov_beta, sigma_my_syst):
    my = beta[0]
    sigma = beta[1]
    A = beta[2]
    C = beta[3]
    sigma_my_sq = sigma_my_syst**2 + np.abs(cov_beta[0, 0])
    sigma_sigma_sq = np.abs(cov_beta[1, 1])
    sigma_A_sq = np.abs(cov_beta[2, 2])
    sigma_C_sq = np.abs(cov_beta[3, 3])

    min = np.abs(x - (my - 3*sigma)).argmin()
    max = np.abs(x - (my + 3*sigma)).argmin()
    x = x[min:max:]
    sigma_x = sigma_x[min:max:]

    exp_fac = np.exp(- (x - my)**2 / (2 * sigma**2))
    dest_ohne_C = A/np.sqrt(2*np.pi * sigma**2) * exp_fac

    value = C + dest_ohne_C

    sigma_value_stat = -(x - my)/(sigma**2) * sigma_x * dest_ohne_C

    sigma_value_syst_sq = sigma_C_sq + (dest_ohne_C/A)**2 * sigma_A_sq + (-2*np.pi*sigma * dest_ohne_C / (2*np.pi* sigma**2) + (x - my)**2 / sigma**3 * dest_ohne_C)**2 * sigma_sigma_sq + ((x - my)/sigma**2 * dest_ohne_C)**2 * sigma_my_sq
    sigma_value_syst = np.sqrt(sigma_value_syst_sq)

    total_value = np.sum(value)
    total_sigma_value_stat = np.sqrt(np.sum(sigma_value_stat**2))
    total_sigma_value_syst = np.sqrt(np.sum(sigma_value_syst**2))
    return (total_value, total_sigma_value_stat, total_sigma_value_syst)

data_50 = parseCSV('50_deg_alu.TKA')[:, 0]
times_50 = data_50[0:2:]
count_50 = data_50[2::]
data_60 = parseCSV('60_deg_alu_absorber_80.TKA')[:, 0]
times_60 = data_60[0:2:]
count_60 = data_60[2::]
data_80 = parseCSV('80_deg_alu.TKA')[:, 0]
times_80 = data_80[0:2:]
count_80 = data_80[2::]
data_105 = parseCSV('105_deg_alu.TKA')[:, 0]
times_105 = data_105[0:2:]
count_105 = data_105[2::]
data_135 = parseCSV('135_deg_alu.TKA')[:, 0]
times_135 = data_135[0:2:]
count_135 = data_135[2::]

data_50_fe = parseCSV('50_deg_fe.TKA')[:, 0]
times_50_fe = data_50_fe[0:2:]
count_50_fe = data_50_fe[2::]
data_60_fe = parseCSV('60_deg_fe_absorber_80.TKA')[:, 0]
times_60_fe = data_60_fe[0:2:]
count_60_fe = data_60_fe[2::]
data_80_fe = parseCSV('80_deg_fe.TKA')[:, 0]
times_80_fe = data_80_fe[0:2:]
count_80_fe = data_80_fe[2::]
data_105_fe = parseCSV('105_deg_fe.TKA')[:, 0]
times_105_fe = data_105_fe[0:2:]
count_105_fe = data_105_fe[2::]
data_135_fe = parseCSV('135_deg_fe.TKA')[:, 0]
times_135_fe = data_135_fe[0:2:]
count_135_fe = data_135_fe[2::]

data_50_rausch = parseCSV('50_deg_rausch.TKA')[:, 0]
times_50_rausch = data_50_rausch[0:2:]
count_50_rausch = data_50_rausch[2::]
data_60_rausch = parseCSV('60_deg_rausch_absorber_80.TKA')[:, 0]
times_60_rausch = data_60_rausch[0:2:]
count_60_rausch = data_60_rausch[2::]
data_80_rausch = parseCSV('80_deg_rausch.TKA')[:, 0]
times_80_rausch = data_80_rausch[0:2:]
count_80_rausch = data_80_rausch[2::]
data_105_rausch = parseCSV('105_deg_rausch.TKA')[:, 0]
times_105_rausch = data_105_rausch[0:2:]
count_105_rausch = data_105_rausch[2::]
data_135_rausch = parseCSV('135_deg_rausch.TKA')[:, 0]
times_135_rausch = data_135_rausch[0:2:]
count_135_rausch = data_135_rausch[2::]

sigma_count_50 = np.sqrt(count_50)
sigma_count_60 = np.sqrt(count_60)
sigma_count_80 = np.sqrt(count_80)
sigma_count_105 = np.sqrt(count_105)
sigma_count_135 = np.sqrt(count_135)

sigma_count_50_fe = np.sqrt(count_50_fe)
sigma_count_60_fe = np.sqrt(count_60_fe)
sigma_count_80_fe = np.sqrt(count_80_fe)
sigma_count_105_fe = np.sqrt(count_105_fe)
sigma_count_135_fe = np.sqrt(count_135_fe)

sigma_count_50_rausch = np.sqrt(count_50_rausch)
sigma_count_60_rausch = np.sqrt(count_60_rausch)
sigma_count_80_rausch = np.sqrt(count_80_rausch)
sigma_count_105_rausch = np.sqrt(count_105_rausch)
sigma_count_135_rausch = np.sqrt(count_135_rausch)

channel_50 = np.array(np.arange(count_50.shape[0]), dtype = 'float64')
channel_60 = np.array(np.arange(count_60.shape[0]), dtype = 'float64')
channel_80 = np.array(np.arange(count_80.shape[0]), dtype = 'float64')
channel_105 = np.array(np.arange(count_105.shape[0]), dtype = 'float64')
channel_135 = np.array(np.arange(count_135.shape[0]), dtype = 'float64')

channel_50_fe = np.array(np.arange(count_50_fe.shape[0]), dtype = 'float64')
channel_60_fe = np.array(np.arange(count_60_fe.shape[0]), dtype = 'float64')
channel_80_fe = np.array(np.arange(count_80_fe.shape[0]), dtype = 'float64')
channel_105_fe = np.array(np.arange(count_105_fe.shape[0]), dtype = 'float64')
channel_135_fe = np.array(np.arange(count_135_fe.shape[0]), dtype = 'float64')

channel_50_rausch = np.array(np.arange(count_50_rausch.shape[0]), dtype = 'float64')
channel_60_rausch = np.array(np.arange(count_60_rausch.shape[0]), dtype = 'float64')
channel_80_rausch = np.array(np.arange(count_80_rausch.shape[0]), dtype = 'float64')
channel_105_rausch = np.array(np.arange(count_105_rausch.shape[0]), dtype = 'float64')
channel_135_rausch = np.array(np.arange(count_135_rausch.shape[0]), dtype = 'float64')

sigma_channel_50 = 1/np.sqrt(12) * np.ones_like(count_50)
sigma_channel_60 = 1/np.sqrt(12) * np.ones_like(count_60)
sigma_channel_80 = 1/np.sqrt(12) * np.ones_like(count_80)
sigma_channel_105 = 1/np.sqrt(12) * np.ones_like(count_105)
sigma_channel_135 = 1/np.sqrt(12) * np.ones_like(count_135)

sigma_channel_50_fe = 1/np.sqrt(12) * np.ones_like(count_50_fe)
sigma_channel_60_fe = 1/np.sqrt(12) * np.ones_like(count_60_fe)
sigma_channel_80_fe = 1/np.sqrt(12) * np.ones_like(count_80_fe)
sigma_channel_105_fe = 1/np.sqrt(12) * np.ones_like(count_105_fe)
sigma_channel_135_fe = 1/np.sqrt(12) * np.ones_like(count_135_fe)

sigma_channel_50_rausch = 1/np.sqrt(12) * np.ones_like(count_50_rausch)
sigma_channel_60_rausch = 1/np.sqrt(12) * np.ones_like(count_60_rausch)
sigma_channel_80_rausch = 1/np.sqrt(12) * np.ones_like(count_80_rausch)
sigma_channel_105_rausch = 1/np.sqrt(12) * np.ones_like(count_105_rausch)
sigma_channel_135_rausch = 1/np.sqrt(12) * np.ones_like(count_135_rausch)

count_50_cleaned = count_50 - count_50_rausch
count_50_cleaned[np.where(count_50_cleaned < 0)] = 0
count_60_cleaned = count_60 - count_60_rausch
count_60_cleaned[np.where(count_60_cleaned < 0)] = 0
count_80_cleaned = count_80 - count_80_rausch
count_80_cleaned[np.where(count_80_cleaned < 0)] = 0
count_105_cleaned = count_105 - count_105_rausch
count_105_cleaned[np.where(count_105_cleaned < 0)] = 0
count_135_cleaned = count_135 - count_135_rausch
count_135_cleaned[np.where(count_135_cleaned < 0)] = 0

count_50_fe_cleaned = count_50_fe - count_50_rausch
count_50_fe_cleaned[np.where(count_50_fe_cleaned < 0)] = 0
count_60_fe_cleaned = count_60_fe - count_60_rausch
count_60_fe_cleaned[np.where(count_60_fe_cleaned < 0)] = 0
count_80_fe_cleaned = count_80_fe - count_80_rausch
count_80_fe_cleaned[np.where(count_80_fe_cleaned < 0)] = 0
count_105_fe_cleaned = count_105_fe - count_105_rausch
count_105_fe_cleaned[np.where(count_105_fe_cleaned < 0)] = 0
count_135_fe_cleaned = count_135_fe - count_135_rausch
count_135_fe_cleaned[np.where(count_135_fe_cleaned < 0)] = 0

sigma_count_50_cleaned = np.sqrt(sigma_count_50**2 + sigma_count_50_rausch**2)
sigma_count_60_cleaned = np.sqrt(sigma_count_60**2 + sigma_count_60_rausch**2)
sigma_count_80_cleaned = np.sqrt(sigma_count_80**2 + sigma_count_80_rausch**2)
sigma_count_105_cleaned = np.sqrt(sigma_count_105**2 + sigma_count_105_rausch**2)
sigma_count_135_cleaned = np.sqrt(sigma_count_135**2 + sigma_count_135_rausch**2)

sigma_count_50_fe_cleaned = np.sqrt(sigma_count_50_fe**2 + sigma_count_50_rausch**2)
sigma_count_60_fe_cleaned = np.sqrt(sigma_count_60_fe**2 + sigma_count_60_rausch**2)
sigma_count_80_fe_cleaned = np.sqrt(sigma_count_80_fe**2 + sigma_count_80_rausch**2)
sigma_count_105_fe_cleaned = np.sqrt(sigma_count_105_fe**2 + sigma_count_105_rausch**2)
sigma_count_135_fe_cleaned = np.sqrt(sigma_count_135_fe**2 + sigma_count_135_rausch**2)

energy_50 = channel_50 * energy_scale + energy_offset
sigma_energy_50 = sigma_channel_50 * energy_scale
energy_60 = channel_60 * energy_scale + energy_offset
sigma_energy_60 = sigma_channel_60 * energy_scale
energy_80 = channel_80 * energy_scale + energy_offset
sigma_energy_80 = sigma_channel_80 * energy_scale
energy_105 = channel_105 * energy_scale + energy_offset
sigma_energy_105 = sigma_channel_105 * energy_scale
energy_135 = channel_135 * energy_scale + energy_offset
sigma_energy_135 = sigma_channel_135 * energy_scale

energy_50_fe = channel_50_fe * energy_scale + energy_offset
sigma_energy_50_fe = sigma_channel_50_fe * energy_scale
energy_60_fe = channel_60_fe * energy_scale + energy_offset
sigma_energy_60_fe = sigma_channel_60_fe * energy_scale
energy_80_fe = channel_80_fe * energy_scale + energy_offset
sigma_energy_80_fe = sigma_channel_80_fe * energy_scale
energy_105_fe = channel_105_fe * energy_scale + energy_offset
sigma_energy_105_fe = sigma_channel_105_fe * energy_scale
energy_135_fe = channel_135_fe * energy_scale + energy_offset
sigma_energy_135_fe = sigma_channel_135_fe * energy_scale

sigma_energy_50_syst = np.sqrt((channel_50 * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_60_syst = np.sqrt((channel_60 * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_80_syst = np.sqrt((channel_80 * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_105_syst = np.sqrt((channel_105 * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_135_syst = np.sqrt((channel_135 * sigma_energy_scale)**2 + sigma_energy_offset**2)

sigma_energy_50_fe_syst = np.sqrt((channel_50_fe * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_60_fe_syst = np.sqrt((channel_60_fe * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_80_fe_syst = np.sqrt((channel_80_fe * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_105_fe_syst = np.sqrt((channel_105_fe * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_135_fe_syst = np.sqrt((channel_135_fe * sigma_energy_scale)**2 + sigma_energy_offset**2)

print('##### Rauschen')
errorscale = 1
plt.errorbar(channel_50_rausch, count_50_rausch, xerr = sigma_channel_50_rausch * errorscale, yerr = sigma_count_50_rausch * errorscale, label = r'Noise for Position $\theta = 50 \degree$', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'Channel')
plt.title(r'Data Noise at $\theta = 50 \degree$')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/noise_50_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(channel_60_rausch, count_60_rausch, xerr = sigma_channel_60_rausch * errorscale, yerr = sigma_count_60_rausch * errorscale, label = r'Noise for Position $\theta = 60 \degree$', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'Channel')
plt.title(r'Data Noise at $\theta = 60 \degree$')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/noise_60_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(channel_80_rausch, count_80_rausch, xerr = sigma_channel_80_rausch * errorscale, yerr = sigma_count_80_rausch * errorscale, label = r'Noise for Position $\theta = 80 \degree$', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'Channel')
plt.title(r'Data Noise at $\theta = 80 \degree$')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/noise_80_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(channel_105_rausch, count_105_rausch, xerr = sigma_channel_105_rausch * errorscale, yerr = sigma_count_105_rausch * errorscale, label = r'Noise for Position $\theta = 105 \degree$', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'Channel')
plt.title(r'Data Noise at $\theta = 105 \degree$')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/noise_105_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(channel_135_rausch, count_135_rausch, xerr = sigma_channel_135_rausch * errorscale, yerr = sigma_count_135_rausch * errorscale, label = r'Noise for Position $\theta = 135 \degree$', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'Channel')
plt.title(r'Data Noise at $\theta = 135 \degree$')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/noise_135_conv.pdf')
plt.clf()

print('##### Daten, korrigiert')
print('### Fe')
errorscale = 1
plt.errorbar(energy_50_fe, count_50_fe_cleaned, xerr = sigma_energy_50_fe, yerr = sigma_count_50_fe_cleaned, label = r'Count for $\theta = 50$°, Fe', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=50$°, Fe, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_50_fe_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(energy_60_fe, count_60_fe_cleaned, xerr = sigma_energy_60_fe, yerr = sigma_count_60_fe_cleaned, label = r'Count for $\theta = 60$°, Fe', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=60$°, Fe, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_60_fe_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(energy_80_fe, count_80_fe_cleaned, xerr = sigma_energy_80_fe, yerr = sigma_count_80_fe_cleaned, label = r'Count for $\theta = 80$°, Fe', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=80$°, Fe, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_80_fe_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(energy_105_fe, count_105_fe_cleaned, xerr = sigma_energy_105_fe, yerr = sigma_count_105_fe_cleaned, label = r'Count for $\theta = 105$°, Fe', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=105$°, Fe, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_105_fe_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(energy_135_fe, count_135_fe_cleaned, xerr = sigma_energy_135_fe, yerr = sigma_count_135_fe_cleaned, label = r'Count for $\theta = 135$°, Fe', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=135$°, Fe, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_135_fe_conv.pdf')
plt.clf()

print('### Al')
errorscale = 1
plt.errorbar(energy_50, count_50_cleaned, xerr = sigma_energy_50, yerr = sigma_count_50_cleaned, label = r'Count for $\theta = 50$°, Al', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=50$°, Al, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_50_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(energy_60, count_60_cleaned, xerr = sigma_energy_60, yerr = sigma_count_60_cleaned, label = r'Count for $\theta = 60$°, Al', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=60$°, Al, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_60_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(energy_80, count_80_cleaned, xerr = sigma_energy_80, yerr = sigma_count_80_cleaned, label = r'Count for $\theta = 80$°, Al', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=80$°, Al, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_80_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(energy_105, count_105_cleaned, xerr = sigma_energy_105, yerr = sigma_count_105_cleaned, label = r'Count for $\theta = 105$°, Al', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=105$°, Al, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_105_conv.pdf')
plt.clf()

errorscale = 1
plt.errorbar(energy_135, count_135_cleaned, xerr = sigma_energy_135, yerr = sigma_count_135_cleaned, label = r'Count for $\theta = 135$°, Al', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \, [keV]$')
plt.title(r'Data Counts $\theta=135$°, Al, conv.')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_135_conv.pdf')
plt.clf()

thetas_fe = []
sigma_thetas_fe = []
energies_fe = []
sigma_energies_fe = []
sigma_energies_syst_fe = []
peak_count_fe = []

print('##### Fits')
print('### Fe')
print('# theta = 50°')
min = np.abs(energy_50_fe - 400).argmin()
max = np.abs(energy_50_fe - 520).argmin()
x = energy_50_fe[min:max:]
y = count_50_fe_cleaned[min:max:]
sigma_x = sigma_energy_50_fe[min:max:]
sigma_x_syst = sigma_energy_50_fe_syst[min:max:]
sigma_y = sigma_count_50_fe_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 480, sigma0 = 20, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas_fe.append(theta_50)
sigma_thetas_fe.append(sigma_theta_50)
energies_fe.append(output.beta[0])
sigma_energies_fe.append(output.cov_beta[0, 0])
sigma_energies_syst_fe.append(sigma_my_syst)
peak_count_fe.append(totalCount(energy_50_fe, sigma_energy_50_fe, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 50\degree$, Fe, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_50_fit_fe.pdf')
plt.clf()

print('# theta = 60°')
min = np.abs(energy_60_fe - 350).argmin()
max = np.abs(energy_60_fe - 470).argmin()
x = energy_60_fe[min:max:]
y = count_60_fe_cleaned[min:max:]
sigma_x = sigma_energy_60_fe[min:max:]
sigma_x_syst = sigma_energy_60_fe_syst[min:max:]
sigma_y = sigma_count_60_fe_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 410, sigma0 = 20, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas_fe.append(theta_60)
sigma_thetas_fe.append(sigma_theta_60)
energies_fe.append(output.beta[0])
sigma_energies_fe.append(output.cov_beta[0, 0])
sigma_energies_syst_fe.append(sigma_my_syst)
peak_count_fe.append(totalCount(energy_60_fe, sigma_energy_60_fe, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 60\degree$, Fe, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_60_fit_fe.pdf')
plt.clf()

print('# theta = 80°')
min = np.abs(energy_80_fe - 280).argmin()
max = np.abs(energy_80_fe - 380).argmin()
x = energy_80_fe[min:max:]
y = count_80_fe_cleaned[min:max:]
sigma_x = sigma_energy_80_fe[min:max:]
sigma_x_syst = sigma_energy_80_fe_syst[min:max:]
sigma_y = sigma_count_80_fe_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 310, sigma0 = 20, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas_fe.append(theta_80)
sigma_thetas_fe.append(sigma_theta_80)
energies_fe.append(output.beta[0])
sigma_energies_fe.append(output.cov_beta[0, 0])
sigma_energies_syst_fe.append(sigma_my_syst)
peak_count_fe.append(totalCount(energy_80_fe, sigma_energy_80_fe, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 80\degree$, Fe, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_80_fit_fe.pdf')
plt.clf()

print('# theta = 105°')
min = np.abs(energy_105_fe - 220).argmin()
max = np.abs(energy_105_fe - 300).argmin()
x = energy_105_fe[min:max:]
y = count_105_fe_cleaned[min:max:]
sigma_x = sigma_energy_105_fe[min:max:]
sigma_x_syst = sigma_energy_105_fe_syst[min:max:]
sigma_y = sigma_count_105_fe_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 250, sigma0 = 20, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas_fe.append(theta_105)
sigma_thetas_fe.append(sigma_theta_105)
energies_fe.append(output.beta[0])
sigma_energies_fe.append(output.cov_beta[0, 0])
sigma_energies_syst_fe.append(sigma_my_syst)
peak_count_fe.append(totalCount(energy_105_fe, sigma_energy_105_fe, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 105\degree$, Fe, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_105_fit_fe.pdf')
plt.clf()

print('# theta = 135°')
min = np.abs(energy_105_fe - 160).argmin()
max = np.abs(energy_105_fe - 240).argmin()
x = energy_135_fe[min:max:]
y = count_135_fe_cleaned[min:max:]
sigma_x = sigma_energy_135_fe[min:max:]
sigma_x_syst = sigma_energy_135_fe_syst[min:max:]
sigma_y = sigma_count_135_fe_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 220, sigma0 = 20, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas_fe.append(theta_135)
sigma_thetas_fe.append(sigma_theta_135)
energies_fe.append(output.beta[0])
sigma_energies_fe.append(output.cov_beta[0, 0])
sigma_energies_syst_fe.append(sigma_my_syst)
peak_count_fe.append(totalCount(energy_135_fe, sigma_energy_135_fe, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 135\degree$, Fe, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_135_fit_fe.pdf')
plt.clf()

thetas = []
sigma_thetas = []
energies = []
sigma_energies = []
sigma_energies_syst = []
peak_count = []

print('### Al')
print('# theta = 50°')
min = np.abs(energy_50 - 400).argmin()
max = np.abs(energy_50 - 520).argmin()
x = energy_50[min:max:]
y = count_50_cleaned[min:max:]
sigma_x = sigma_energy_50[min:max:]
sigma_x_syst = sigma_energy_50_syst[min:max:]
sigma_y = sigma_count_50_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 480, sigma0 = 20, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_50)
sigma_thetas.append(sigma_theta_50)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_50, sigma_energy_50, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 50\degree$, Al, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_50_fit.pdf')
plt.clf()

print('# theta = 60°')
min = np.abs(energy_60 - 330).argmin()
max = np.abs(energy_60 - 520).argmin()
x = energy_60[min:max:]
y = count_60_cleaned[min:max:]
sigma_x = sigma_energy_60[min:max:]
sigma_x_syst = sigma_energy_60_syst[min:max:]
sigma_y = sigma_count_60_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 410, sigma0 = 20, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_60)
sigma_thetas.append(sigma_theta_60)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_60, sigma_energy_60, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 60\degree$, Al, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_60_fit.pdf')
plt.clf()

print('# theta = 80°')
min = np.abs(energy_80 - 280).argmin()
max = np.abs(energy_80 - 380).argmin()
x = energy_80[min:max:]
y = count_80_cleaned[min:max:]
sigma_x = sigma_energy_80[min:max:]
sigma_x_syst = sigma_energy_80_syst[min:max:]
sigma_y = sigma_count_80_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 310, sigma0 = 20, A0 = A0_from_max(150, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_80)
sigma_thetas.append(sigma_theta_80)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_80, sigma_energy_80, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 80\degree$, Al, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_80_fit.pdf')
plt.clf()

print('# theta = 105°')
min = np.abs(energy_105 - 200).argmin()
max = np.abs(energy_105 - 300).argmin()
x = energy_105[min:max:]
y = count_105_cleaned[min:max:]
sigma_x = sigma_energy_105[min:max:]
sigma_x_syst = sigma_energy_105_syst[min:max:]
sigma_y = sigma_count_105_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 270, sigma0 = 20, A0 = A0_from_max(200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_105)
sigma_thetas.append(sigma_theta_105)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_105, sigma_energy_105, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 105\degree$, Al, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_105_fit.pdf')
plt.clf()

print('# theta = 135°')
min = np.abs(energy_105 - 180).argmin()
max = np.abs(energy_105 - 230).argmin()
x = energy_135[min:max:]
y = count_135_cleaned[min:max:]
sigma_x = sigma_energy_135[min:max:]
sigma_x_syst = sigma_energy_135_syst[min:max:]
sigma_y = sigma_count_135_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 200, sigma0 = 20, A0 = A0_from_max(200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_135)
sigma_thetas.append(sigma_theta_135)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_135, sigma_energy_135, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 135\degree$, Al, conv.')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/conv_135_fit.pdf')
plt.clf()

print('##### additional analysis')

print('### Format and log energy information ')
thetas = np.array(thetas, dtype = 'float64')
sigma_thetas = np.array(sigma_thetas, dtype = 'float64')
energies = np.array(energies, dtype = 'float64')
sigma_energies = np.array(sigma_energies, dtype = 'float64')
sigma_energies_syst = np.array(sigma_energies_syst, dtype = 'float64')

thetas_fe = np.array(thetas_fe, dtype = 'float64')
sigma_thetas_fe = np.array(sigma_thetas_fe, dtype = 'float64')
energies_fe = np.array(energies_fe, dtype = 'float64')
sigma_energies_fe = np.array(sigma_energies_fe, dtype = 'float64')
sigma_energies_syst_fe = np.array(sigma_energies_syst_fe, dtype = 'float64')
peak_count = np.array(peak_count, dtype = 'float64')
peak_count_fe = np.array(peak_count_fe, dtype = 'float64')

file = open('./output/conv.log', 'w')
for i in range(len(thetas)):
    file.write(f'{thetas[i]}\t{energies[i]}\t{sigma_thetas[i]}\t{sigma_energies[i]}\t{sigma_energies_syst[i]}\t{peak_count[i, 0]}\t{peak_count[i, 1]}\t{peak_count[i, 2]}\n')
file.close()

file = open('./output/conv_fe.log', 'w')
for i in range(len(thetas_fe)):
    file.write(f'{thetas_fe[i]}\t{energies_fe[i]}\t{sigma_thetas_fe[i]}\t{sigma_energies_fe[i]}\t{sigma_energies_syst_fe[i]}\t{peak_count_fe[i, 0]}\t{peak_count_fe[i, 1]}\t{peak_count_fe[i, 2]}\n')
file.close()
