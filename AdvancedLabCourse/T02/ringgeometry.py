import numpy as np
import matplotlib.pyplot as plt
import scipy.odr as sodr
import scipy.stats as scs
from my_methods import *
import scipy.optimize

energy_scale = 1.6473 #keV
sigma_energy_scale = 0.0011 #keV
energy_offset = -27 #keV
sigma_energy_offset = 0.5 #keV

sigma_x = 0.1/np.sqrt(12)
sigma_R = 0.005/np.sqrt(12)
sigma_a_1 = 0.01/np.sqrt(12)

def totalCount(x, sigma_x, beta, cov_beta, sigma_my_syst):
    my = beta[0]
    sigma = beta[1]
    A = beta[2]
    C = beta[3]
    sigma_my_sq = sigma_my_syst**2 + np.abs(cov_beta[0, 0])
    sigma_sigma_sq = np.abs(cov_beta[1, 1])
    sigma_A_sq = np.abs(cov_beta[2, 2])
    sigma_C_sq = np.abs(cov_beta[3, 3])

    min = np.abs(x - (my - 3*sigma)).argmin()
    max = np.abs(x - (my + 3*sigma)).argmin()
    x = x[min:max:]
    sigma_x = sigma_x[min:max:]

    exp_fac = np.exp(- (x - my)**2 / (2 * sigma**2))
    dest_ohne_C = A/np.sqrt(2*np.pi * sigma**2) * exp_fac

    value = C + dest_ohne_C

    sigma_value_stat = -(x - my)/(sigma**2) * sigma_x * dest_ohne_C

    sigma_value_syst_sq = sigma_C_sq + (dest_ohne_C/A)**2 * sigma_A_sq + (-2*np.pi*sigma * dest_ohne_C / (2*np.pi* sigma**2) + (x - my)**2 / sigma**3 * dest_ohne_C)**2 * sigma_sigma_sq + ((x - my)/sigma**2 * dest_ohne_C)**2 * sigma_my_sq
    sigma_value_syst = np.sqrt(sigma_value_syst_sq)

    total_value = np.sum(value)
    total_sigma_value_stat = np.sqrt(np.sum(sigma_value_stat**2))
    total_sigma_value_syst = np.sqrt(np.sum(sigma_value_syst**2))
    return (total_value, total_sigma_value_stat, total_sigma_value_syst)

x_0 = 50
x_2_0 = 66.6
x_2_1 = 121.5
x_2_2 = x_2_1
x_4_0 = 113.6
x_4_1 = 168.5
x_4_2 = 211.5
a_1_0 = 3.36
a_1_1 = 3.5
a_1_2 = a_1_1
R_k = 6.745
R_m = 9.1975
R_g = 11.69

def theta(x_2, x_4, a_1, R):
    a = (x_4 - x_2) - a_1 - 4.33 + 2.5/2
    sigma_a = np.sqrt(2*sigma_x**2 + sigma_a_1**2 + (0.01/np.sqrt(12))**2 + (0.01/2 /np.sqrt(12))**2)
    b = (x_2 - x_0) - 2.5 - 2.06/2 + 4.33
    sigma_b = np.sqrt(2*sigma_x**2 + 3*(0.01/np.sqrt(12))**2 + 3*(0.01/2 /np.sqrt(12))**2)
    c = R
    sigma_c = sigma_R

    theta = np.arctan(c/a) + np.arctan(c/b)
    sigma_theta = np.sqrt((c*sigma_a / (a**2 + c**2))**2 + (c*sigma_b / (b**2 + c**2))**2 + (a/(a**2 + c**2) + b/(b**2 + c**2))**2 * sigma_c**2)

    print(f'\t an{round(a, sigma_a)}en an{round(b, sigma_b)}en an{round(theta*180/np.pi, sigma_theta*180/np.pi)}en')
    return (theta * 180 / np.pi, sigma_theta * 180 / np.pi)

theta_50, sigma_theta_50 = theta(x_2_0, x_4_0, a_1_0, R_g)
theta_40, sigma_theta_40 = theta(x_2_0, x_4_0, a_1_0, R_m)
theta_30, sigma_theta_30 = theta(x_2_0, x_4_0, a_1_0, R_k)
theta_20, sigma_theta_20 = theta(x_2_1, x_4_1, a_1_1, R_m)
theta_10, sigma_theta_10 = theta(x_2_2, x_4_2, a_1_2, R_k)

thetas = []
sigma_thetas = []
energies = []
sigma_energies = []
sigma_energies_syst = []
peak_count = []

data_50 = parseCSV('666mm_g_ring.TKA')[:, 0]
times_50 = data_50[0:2:]
count_50 = data_50[2::]
data_40 = parseCSV('666mm_m_ring.TKA')[:, 0]
times_40 = data_40[0:2:]
count_40 = data_40[2::]
data_30 = parseCSV('666mm_k_ring.TKA')[:, 0]
times_30 = data_30[0:2:]
count_30 = data_30[2::]
data_20 = parseCSV('1210mm_m_ring.TKA')[:, 0]
times_20 = data_20[0:2:]
count_20 = data_20[2::]
data_10 = parseCSV('2115mm_k_ring.TKA')[:, 0]
times_10 = data_10[0:2:]
count_10 = data_10[2::]

data_rauschen_0 = parseCSV('666mm_rauschen_ring.TKA')[:, 0]
times_rauschen_0 = data_rauschen_0[0:2:]
count_rauschen_0 = data_rauschen_0[2::]
data_rauschen_1 = parseCSV('1210mm_rauschen_ring.TKA')[:, 0]
times_rauschen_1 = data_rauschen_1[0:2:]
count_rauschen_1 = data_rauschen_1[2::]
data_rauschen_2 = parseCSV('2115mm_rauschen_ring.TKA')[:, 0]
times_rauschen_2 = data_rauschen_2[0:2:]
count_rauschen_2 = data_rauschen_2[2::]

sigma_count_50 = np.sqrt(count_50)
sigma_count_40 = np.sqrt(count_40)
sigma_count_30 = np.sqrt(count_30)
sigma_count_20 = np.sqrt(count_20)
sigma_count_10 = np.sqrt(count_10)
sigma_count_rauschen_0 = np.sqrt(count_rauschen_0)
sigma_count_rauschen_1 = np.sqrt(count_rauschen_1)
sigma_count_rauschen_2 = np.sqrt(count_rauschen_2)

channel_50 = np.array(np.arange(count_50.shape[0]), dtype = 'float64')
channel_40 = np.array(np.arange(count_40.shape[0]), dtype = 'float64')
channel_30 = np.array(np.arange(count_30.shape[0]), dtype = 'float64')
channel_20 = np.array(np.arange(count_20.shape[0]), dtype = 'float64')
channel_10 = np.array(np.arange(count_10.shape[0]), dtype = 'float64')
channel_rauschen_0 = np.array(np.arange(count_rauschen_0.shape[0]), dtype = 'float64')
channel_rauschen_1 = np.array(np.arange(count_rauschen_1.shape[0]), dtype = 'float64')
channel_rauschen_2 = np.array(np.arange(count_rauschen_2.shape[0]), dtype = 'float64')

sigma_channel_50 = 1/np.sqrt(12) * np.ones_like(count_50)
sigma_channel_40 = 1/np.sqrt(12) * np.ones_like(count_40)
sigma_channel_30 = 1/np.sqrt(12) * np.ones_like(count_30)
sigma_channel_20 = 1/np.sqrt(12) * np.ones_like(count_20)
sigma_channel_10 = 1/np.sqrt(12) * np.ones_like(count_10)
sigma_channel_rauschen_0 = 1/np.sqrt(12) * np.ones_like(channel_rauschen_0)
sigma_channel_rauschen_1 = 1/np.sqrt(12) * np.ones_like(channel_rauschen_1)
sigma_channel_rauschen_2 = 1/np.sqrt(12) * np.ones_like(channel_rauschen_2)

count_50_cleaned = count_50 - count_rauschen_0
count_50_cleaned[np.where(count_50_cleaned < 0)] = 0
count_40_cleaned = count_40 - count_rauschen_0
count_40_cleaned[np.where(count_40_cleaned < 0)] = 0
count_30_cleaned = count_30 - count_rauschen_0
count_30_cleaned[np.where(count_30_cleaned < 0)] = 0
count_20_cleaned = count_20 - count_rauschen_1
count_20_cleaned[np.where(count_20_cleaned < 0)] = 0
count_10_cleaned = count_10 - count_rauschen_2
count_10_cleaned[np.where(count_10_cleaned < 0)] = 0

sigma_count_50_cleaned = np.sqrt(sigma_count_50**2 + sigma_count_rauschen_0**2)
sigma_count_40_cleaned = np.sqrt(sigma_count_40**2 + sigma_count_rauschen_0**2)
sigma_count_30_cleaned = np.sqrt(sigma_count_30**2 + sigma_count_rauschen_0**2)
sigma_count_20_cleaned = np.sqrt(sigma_count_20**2 + sigma_count_rauschen_1**2)
sigma_count_10_cleaned = np.sqrt(sigma_count_10**2 + sigma_count_rauschen_2**2)

energy_50 = channel_50 * energy_scale + energy_offset
sigma_energy_50 = sigma_channel_50 * energy_scale
energy_40 = channel_40 * energy_scale + energy_offset
sigma_energy_40 = sigma_channel_40 * energy_scale
energy_30 = channel_30 * energy_scale + energy_offset
sigma_energy_30 = sigma_channel_30 * energy_scale
energy_20 = channel_20 * energy_scale + energy_offset
sigma_energy_20 = sigma_channel_20 * energy_scale
energy_10 = channel_10 * energy_scale + energy_offset
sigma_energy_10 = sigma_channel_10 * energy_scale

sigma_energy_50_syst = np.sqrt((channel_50 * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_40_syst = np.sqrt((channel_40 * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_30_syst = np.sqrt((channel_30 * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_20_syst = np.sqrt((channel_20 * sigma_energy_scale)**2 + sigma_energy_offset**2)
sigma_energy_10_syst = np.sqrt((channel_10 * sigma_energy_scale)**2 + sigma_energy_offset**2)

#'''
print('##### Rauschen')
errorscale = 5
plt.errorbar(channel_rauschen_0, count_rauschen_0, xerr = sigma_channel_rauschen_0 * errorscale, yerr = sigma_count_rauschen_0 * errorscale, label = r'Noise for Position 0', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'Channel')
plt.title(r'Data Noise at Position 0')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/noise_position_0.pdf')
plt.clf()

plt.errorbar(channel_rauschen_1, count_rauschen_1, xerr = sigma_channel_rauschen_1 * errorscale, yerr = sigma_count_rauschen_1 * errorscale, label = r'Noise for Position 0', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'Channel')
plt.title(r'Data Noise at Position 1')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/noise_position_1.pdf')
plt.clf()

plt.errorbar(channel_rauschen_2, count_rauschen_2, xerr = sigma_channel_rauschen_2 * errorscale, yerr = sigma_count_rauschen_2 * errorscale, label = r'Noise for Position 0', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'Channel')
plt.title(r'Data Noise at Position 2')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/noise_position_2.pdf')
plt.clf()

print('##### Daten, korrigiert')
errorscale = 1
plt.errorbar(energy_50, count_50_cleaned, xerr = sigma_energy_50, yerr = sigma_count_50_cleaned, label = r'Count for $\theta = 50$°', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \,  [keV]$')
plt.title(r'Data Counts $\theta=50$°')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_50.pdf')
plt.clf()

plt.errorbar(energy_40, count_40_cleaned, xerr = sigma_energy_40, yerr = sigma_count_40_cleaned, label = r'Count for $\theta = 40$°', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \,  [keV]$')
plt.title(r'Data Counts $\theta=40$°')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_40.pdf')
plt.clf()

plt.errorbar(energy_30, count_30_cleaned, xerr = sigma_energy_30, yerr = sigma_count_30_cleaned, label = r'Count for $\theta = 30$°', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \,  [keV]$')
plt.title(r'Data Counts $\theta=30$°')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_30.pdf')
plt.clf()

plt.errorbar(energy_20, count_20_cleaned, xerr = sigma_energy_20, yerr = sigma_count_20_cleaned, label = r'Count for $\theta = 20$°', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \,  [keV]$')
plt.title(r'Data Counts $\theta=20$°')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_20.pdf')
plt.clf()

plt.errorbar(energy_10, count_10_cleaned, xerr = sigma_energy_10, yerr = sigma_count_10_cleaned, label = r'Count for $\theta = 10$°', linestyle = '', marker = '.')
plt.ylabel(r'# $Counts$')
plt.xlabel(r'$Energy \,  [keV]$')
plt.title(r'Data Counts $\theta=10$°')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig('output/messdaten_10.pdf')
plt.clf()
#'''

print('##### Fits')
print('### theta = 50°')
min = np.abs(energy_50 - 420).argmin()
max = np.abs(energy_50 - 725).argmin()
x = energy_50[min:max:]
y = count_50_cleaned[min:max:]
sigma_x = sigma_energy_50[min:max:]
sigma_x_syst = sigma_energy_50_syst[min:max:]
sigma_y = sigma_count_50_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 475, sigma0 = 50, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_50)
sigma_thetas.append(sigma_theta_50)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_50, sigma_energy_50, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 50\degree$')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/ring_50_fit.pdf')
plt.clf()

print('### theta = 40°')
min = np.abs(energy_40 - 440).argmin()
max = np.abs(energy_40 - 800).argmin()
x = energy_40[min:max:]
y = count_40_cleaned[min:max:]
sigma_x = sigma_energy_40[min:max:]
sigma_x_syst = sigma_energy_40_syst[min:max:]
sigma_y = sigma_count_40_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 520, sigma0 = 20, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_40)
sigma_thetas.append(sigma_theta_40)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_40, sigma_energy_40, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 40\degree$')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/ring_40_fit.pdf')
plt.clf()

print('### theta = 30°')
min = np.abs(energy_40 - 450).argmin()
max = np.abs(energy_40 - 765).argmin()
x = energy_30[min:max:]
y = count_30_cleaned[min:max:]
sigma_x = sigma_energy_30[min:max:]
sigma_x_syst = sigma_energy_30_syst[min:max:]
sigma_y = sigma_count_30_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 600, sigma0 = 20, A0 = A0_from_max(3200, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_30)
sigma_thetas.append(sigma_theta_30)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_30, sigma_energy_30, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 30\degree$')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/ring_30_fit.pdf')
plt.clf()

print('### theta = 20°')
min = np.abs(energy_40 - 560).argmin()
max = np.abs(energy_40 - 670).argmin()
x = energy_20[min:max:]
y = count_20_cleaned[min:max:]
sigma_x = sigma_energy_20[min:max:]
sigma_x_syst = sigma_energy_20_syst[min:max:]
sigma_y = sigma_count_20_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 600, sigma0 = 20, A0 = A0_from_max(600, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_20)
sigma_thetas.append(sigma_theta_20)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_20, sigma_energy_20, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 20\degree$')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/ring_20_fit.pdf')
plt.clf()

print('### theta = 10°')
min = np.abs(energy_40 - 600).argmin()
max = np.abs(energy_40 - 710).argmin()
x = energy_10[min:max:]
y = count_10_cleaned[min:max:]
sigma_x = sigma_energy_10[min:max:]
sigma_x_syst = sigma_energy_10_syst[min:max:]
sigma_y = sigma_count_10_cleaned[min:max:]

output = gaussian_fit(x, y, xerr = sigma_x, yerr = sigma_y, my0 = 660, sigma0 = 20, A0 = A0_from_max(100, 20))
output_a = gaussian_fit(x + sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])
output_b = gaussian_fit(x - sigma_x_syst, y, xerr = sigma_x, yerr = sigma_y, my0 = output.beta[0], sigma0 = output.beta[1], A0 = output.beta[2])

chiq_dof, p_value = chi_sq_dof_p(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)
sigma_my_syst = np.abs(output_b.beta[0] - output_a.beta[0]) / 2
error_2d = calc_2d_error(x, y, xerr = sigma_x, yerr = sigma_y, beta = output.beta, model = offsetGauss)

thetas.append(theta_10)
sigma_thetas.append(sigma_theta_10)
energies.append(output.beta[0])
sigma_energies.append(output.cov_beta[0, 0])
sigma_energies_syst.append(sigma_my_syst)
peak_count.append(totalCount(energy_10, sigma_energy_10, output.beta, output.cov_beta, sigma_my_syst))

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(x, offsetGauss(output.beta, x), label = r'fit $y = C + \frac{A}{\sqrt{2\pi \sigma^2}}e^{-\frac{(x - \mu)^2}{2 \sigma^2}}$', color='C1')
ax[0].errorbar(x, y, xerr = sigma_x, yerr = sigma_y, label='data', linestyle = '', marker = ',')
ax[1].errorbar(x, y - offsetGauss(output.beta, x), yerr = error_2d, label = 'residuals', linestyle = '', marker = ',')
ax[0].set(ylabel = r'# $Counts$', title = r'Fit for $\theta = 10\degree$')
ax[1].set(xlabel = r'$Energy \, [keV]$', ylabel = r'Data - Fit [# Counts]')
ax[0].text(1.04, 0.32, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))}' + r' $\pm$ ' + f'{np.around(sigma_my_syst, 2)} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[3], np.sqrt(output.cov_beta[3, 3]))}\n' + r'$\frac{\chi^2}{dof}$: ' + f'{np.around(chiq_dof, 2)}, ' + r'$p$: ' + f'{np.around(p_value, 2)}' , horizontalalignment='left', verticalalignment='center', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[0].text(0.5, 0.045, r'$\mu$:'+f' {round(output.beta[0], np.sqrt(output.cov_beta[0, 0]))} (stat.)' + r' $\pm$ ' + f'{sigma_my_syst} (syst.)\n' + r'$\sigma$: ' + f'{round(output.beta[1], np.sqrt(output.cov_beta[1, 1]))}\n' + r'$A$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}\n' + r'$C$: ' + f'{round(output.beta[2], np.sqrt(output.cov_beta[2, 2]))}' , horizontalalignment='right', verticalalignment='bottom', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
#ax[1].set_xlim(-np.max(I_b_10000*1000)*0.02, np.max(I_b_10000*1000)*1.15)
ax[0].grid()
ax[1].grid()
ax[0].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
ax[1].legend(loc = 'upper left', bbox_to_anchor=(1, 1))
fig.tight_layout()
fig.savefig(f'./output/ring_10_fit.pdf')
plt.clf()

print('##### additional analysis')

print('### Format and log energy information ')
thetas = np.array(thetas, dtype = 'float64')
sigma_thetas = np.array(sigma_thetas, dtype = 'float64')
energies = np.array(energies, dtype = 'float64')
sigma_energies = np.array(sigma_energies, dtype = 'float64')
sigma_energies_syst = np.array(sigma_energies_syst, dtype = 'float64')
peak_count = np.array(peak_count, dtype = 'float64')

file = open('./output/ring.log', 'w')
for i in range(len(thetas)):
    file.write(f'{thetas[i]}\t{energies[i]}\t{sigma_thetas[i]}\t{sigma_energies[i]}\t{sigma_energies_syst[i]}\t{peak_count[i, 0]}\t{peak_count[i, 1]}\t{peak_count[i, 2]}\n')
file.close()
