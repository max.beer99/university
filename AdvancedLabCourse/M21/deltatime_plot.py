import numpy as np
import matplotlib.pyplot as plt
import scipy.odr as sodr
import scipy.stats as scs
import scipy.constants as scc
from hist_parse import histParse
from sigfig import round as sigfigRound

def round(value, sigma, notation_scientific = False):
    return sigfigRound(float(value), float(sigma), cutoff = 29, notation = 'sci' if notation_scientific else 'std')

def gauss(B, x):
    # B[0] = my ; B[1] = sigma
    return B[3] + B[2]/np.sqrt(2*np.pi*B[1]**2) * np.exp(-(x-B[0])**2/(2*B[1]**2))

def gaussian_fit(xdata, ydata, xerr, yerr, my0 = 0, sigma0 = 1, A0 = 1, offset0 = 0):
    model = sodr.Model(gauss)
    data = sodr.RealData(xdata, ydata, sx = xerr, sy = yerr)
    odr = sodr.ODR(data, model, beta0=[my0, sigma0, A0, offset0])
    return odr.run()

D = 28.12
sigma_D = 0.029

# data collection
data_max = histParse('DELTATIME_Maximum_2_2.log')
data_random_1 = histParse('DELTATIME_Random_1.log')
data_random_2 = histParse('DELTATIME_Random_2.log')
data_random_3 = histParse('DELTATIME_Random_3.log')

sigma_max_count = np.sqrt(data_max[:, 1])
sigma_random_1_count = np.sqrt(data_random_1[:, 1])
sigma_random_2_count = np.sqrt(data_random_2[:, 1])
sigma_random_3_count = np.sqrt(data_random_3[:, 1])

print(f'\n\tn_max: {np.sum(data_max[:, 1])}')
print(f'\tn_random_1: {np.sum(data_random_1[:, 1])}')
print(f'\tn_random_2: {np.sum(data_random_2[:, 1])}')
print(f'\tn_random_3: {np.sum(data_random_3[:, 1])}\n')

sigma_max_count[np.where(sigma_max_count == 0)] = 1
sigma_random_1_count[np.where(sigma_random_1_count == 0)] = 1
sigma_random_2_count[np.where(sigma_random_2_count == 0)] = 1
sigma_random_3_count[np.where(sigma_random_3_count == 0)] = 1

sigma_max_time = np.ones_like(data_max[:, 0]) * (data_max[1, 0] - data_max[0, 0]) / np.sqrt(12)
sigma_random_1_time = np.ones_like(data_random_1[:, 0]) * (data_random_1[1, 0] - data_random_1[0, 0]) / np.sqrt(12)
sigma_random_2_time = np.ones_like(data_random_2[:, 0]) * (data_random_2[1, 0] - data_random_2[0, 0]) / np.sqrt(12)
sigma_random_3_time = np.ones_like(data_random_3[:, 0]) * (data_random_3[1, 0] - data_random_3[0, 0]) / np.sqrt(12)

print('////////// position: max //////////')
i_min = np.max(np.where(data_max[:, 0] < -300))
i_max = np.max(np.where(data_max[:, 0] < 400))
gaussian = gaussian_fit(data_max[i_min:i_max:, 0], data_max[i_min:i_max:, 1], sigma_max_time, sigma_max_count, my0 = 30, sigma0 = 166, A0 = np.max(data_max[:, 1])*np.sqrt(2*np.pi * 166**2))
print(f'{gaussian.beta=}')
print(f'errors: {np.sqrt(gaussian.cov_beta[0, 0])}, {np.sqrt(gaussian.cov_beta[1, 1])}, {np.sqrt(gaussian.cov_beta[2, 2])}, {np.sqrt(gaussian.cov_beta[1, 1])}')
#print(f'{gaussian.res_var=}')
chi_sq = np.sum(((data_max[i_min:i_max:, 1] - gauss(gaussian.beta, data_max[i_min:i_max:, 0]))/sigma_max_count[i_min:i_max:])**2)
dof = i_max - i_min
print(f'chiq/dof: {chi_sq/dof}')
p = 1 - scs.chi2.cdf(chi_sq, dof)
print(f'{p=}')

# FWHM of gaussian
FWHM_t = 2* np.sqrt(2*np.log(2)) * gaussian.beta[1]
sigma_FWHM_t = 2* np.sqrt(2*np.log(2)) * np.sqrt(gaussian.cov_beta[1, 1])
sigma_t = FWHM_t / np.sqrt(12)
sigma_sigma_t = sigma_FWHM_t / np.sqrt(12)
print(f'\n\t FWHM: {round(FWHM_t, sigma_FWHM_t)} ps\n')

delta_t = gaussian.beta[0]
sigma_delta_t = sigma_t
delta_x = scc.c * delta_t * 1e-10
sigma_delta_x = scc.c * sigma_delta_t * 1e-10
d_1 = (D + delta_x)/2
sigma_d_1 = 0.5 * np.sqrt((sigma_D)**2 + sigma_delta_x**2)
print(f'\n\t delta t: {round(delta_t, sigma_delta_t)} ps')
print(f'\t delta x: {round(delta_x, sigma_delta_x)} cm')
print(f'\t d_1: {round(d_1, sigma_d_1)} cm \n')

plt.errorbar(data_max[:, 0], data_max[:, 1], yerr = sigma_max_count, xerr = sigma_max_time, linestyle = '', marker = ',', label = 'data')
plt.xlabel(r'$\Delta t$ [ps]')
plt.ylabel('# Counts per second')
plt.title('Time difference spectrum (position: max)')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig(f'./output/delta_t_max.pdf')
plt.cla()

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(data_max[i_min:i_max:, 0], gauss(gaussian.beta, data_max[i_min:i_max:, 0]), label='fit', color='C1')
ax[0].errorbar(data_max[i_min:i_max:, 0], data_max[i_min:i_max:, 1], label='data', linestyle = '', marker = ',', yerr = sigma_max_count[i_min:i_max:], xerr = sigma_max_time[i_min:i_max:])
ax[1].errorbar(data_max[i_min:i_max:, 0], data_max[i_min:i_max:, 1] - gauss(gaussian.beta, data_max[i_min:i_max:, 0]), yerr = sigma_max_count[i_min:i_max:], label='residuals', linestyle = '', marker = '.')
ax[1].hlines(0, data_max[i_min, 0], data_max[i_max, 0], color = 'C1')
ax[0].set(ylabel = '# Counts per second', title = 'Time difference spectrum fit (position: max)')
ax[1].set(xlabel = r'$\Delta t$ [ps]', ylabel = r'Data - Fit [$\frac{\#}{s}$]')
ax[0].text(0.015, 0.955, f'µ: {round(gaussian.beta[0], np.sqrt(gaussian.cov_beta[0, 0]))}\nsigma: {round(gaussian.beta[1], np.sqrt(gaussian.cov_beta[1, 1]))}\nchiq/dof: {np.around(chi_sq/dof, 2)}\np: {sigfigRound(float(p), sigfigs = 3)}', horizontalalignment='left', verticalalignment='top', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[0].grid()
ax[1].grid()
ax[0].legend()
ax[1].legend()
fig.tight_layout()
fig.savefig(f'./output/delta_t_max_fit.pdf')
plt.clf()

print('////////// position: random //////////')
plt.errorbar(data_random_1[:, 0], data_random_1[:, 1], yerr = sigma_random_1_count, xerr = sigma_random_1_time, linestyle = '', marker = ',', label = 'random 1', color = 'C0')
plt.errorbar(data_random_2[:, 0], data_random_2[:, 1], yerr = sigma_random_2_count, xerr = sigma_random_2_time, linestyle = '', marker = ',', label = 'random 2', color = 'C1')
plt.errorbar(data_random_3[:, 0], data_random_3[:, 1], yerr = sigma_random_3_count, xerr = sigma_random_3_time, linestyle = '', marker = ',', label = 'random 3', color = 'C2')
plt.xlabel(r'$\Delta t$ [ps]')
plt.ylabel('# Counts per second')
plt.title('Time difference spectrum (position: random)')
plt.grid()
plt.legend()
plt.tight_layout()
plt.savefig(f'./output/delta_t_random.pdf')
plt.cla()

print('////////// position: random 1 //////////')
i_min = np.max(np.where(data_random_1[:, 0] < -300))
i_max = np.max(np.where(data_random_1[:, 0] < 500))
gaussian = gaussian_fit(data_random_1[i_min:i_max:, 0], data_random_1[i_min:i_max:, 1], sigma_random_1_time, sigma_random_1_count, my0 = 30, sigma0 = 166, A0 = np.max(data_random_1[:, 1])*np.sqrt(2*np.pi * 166**2))
print(f'{gaussian.beta=}')
print(f'errors: {np.sqrt(gaussian.cov_beta[0, 0])}, {np.sqrt(gaussian.cov_beta[1, 1])}, {np.sqrt(gaussian.cov_beta[2, 2])}, {np.sqrt(gaussian.cov_beta[1, 1])}')
#print(f'{gaussian.res_var=}')
chi_sq = np.sum(((data_random_1[i_min:i_max:, 1] - gauss(gaussian.beta, data_random_1[i_min:i_max:, 0]))/sigma_random_1_count[i_min:i_max:])**2)
dof = i_max - i_min
print(f'chiq/dof: {chi_sq/dof}')
p = 1 - scs.chi2.cdf(chi_sq, dof)
print(f'{p=}')

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(data_random_1[i_min:i_max:, 0], gauss(gaussian.beta, data_random_1[i_min:i_max:, 0]), label='fit', color='C1')
ax[0].errorbar(data_random_1[i_min:i_max:, 0], data_random_1[i_min:i_max:, 1], label='data', linestyle = '', marker = ',', yerr = sigma_random_1_count[i_min:i_max:], xerr = sigma_random_1_time[i_min:i_max:])
ax[1].errorbar(data_random_1[i_min:i_max:, 0], data_random_1[i_min:i_max:, 1] - gauss(gaussian.beta, data_random_1[i_min:i_max:, 0]), yerr = sigma_random_1_count[i_min:i_max:], label='residuals', linestyle = '', marker = '.')
ax[1].hlines(0, data_random_1[i_min, 0], data_random_1[i_max, 0], color = 'C1')
ax[0].set(ylabel = '# Counts per second', title = 'Time difference spectrum fit (position: random 1)')
ax[1].set(xlabel = r'$\Delta t$ [ps]', ylabel = r'Data - Fit [$\frac{\#}{s}$]')
ax[0].text(0.015, 0.955, f'µ: {round(gaussian.beta[0], np.sqrt(gaussian.cov_beta[0, 0]))}\nsigma: {round(gaussian.beta[1], np.sqrt(gaussian.cov_beta[1, 1]))}\nchiq/dof: {np.around(chi_sq/dof, 2)}\np: {sigfigRound(float(p), sigfigs = 3)}', horizontalalignment='left', verticalalignment='top', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[0].grid()
ax[1].grid()
ax[0].legend()
ax[1].legend()
fig.tight_layout()
fig.savefig(f'./output/delta_t_random_1_fit.pdf')
plt.clf()

# delta time and delta x
delta_t = gaussian.beta[0]
sigma_delta_t = sigma_t
delta_x = scc.c * delta_t * 1e-10
sigma_delta_x = scc.c * sigma_delta_t * 1e-10
d_1 = (D + delta_x)/2
sigma_d_1 = 0.5 * np.sqrt((sigma_D)**2 + sigma_delta_x**2)
print(f'\n\t delta t: {round(delta_t, sigma_delta_t)} ps')
print(f'\t delta x: {round(delta_x, sigma_delta_x)} cm')
print(f'\t d_1: {round(d_1, sigma_d_1)} cm \n')

print('////////// position: random 2 //////////')
i_min = np.max(np.where(data_random_2[:, 0] < -500))
i_max = np.max(np.where(data_random_2[:, 0] < 200))
gaussian = gaussian_fit(data_random_2[i_min:i_max:, 0], data_random_2[i_min:i_max:, 1], sigma_random_2_time, sigma_random_2_count, my0 = 30, sigma0 = 166, A0 = np.max(data_random_2[:, 1])*np.sqrt(2*np.pi * 166**2))
print(f'{gaussian.beta=}')
print(f'errors: {np.sqrt(gaussian.cov_beta[0, 0])}, {np.sqrt(gaussian.cov_beta[1, 1])}, {np.sqrt(gaussian.cov_beta[2, 2])}, {np.sqrt(gaussian.cov_beta[1, 1])}')
#print(f'{gaussian.res_var=}')
chi_sq = np.sum(((data_random_2[i_min:i_max:, 1] - gauss(gaussian.beta, data_random_2[i_min:i_max:, 0]))/sigma_random_2_count[i_min:i_max:])**2)
dof = i_max - i_min
print(f'chiq/dof: {chi_sq/dof}')
p = 1 - scs.chi2.cdf(chi_sq, dof)
print(f'{p=}')

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(data_random_2[i_min:i_max:, 0], gauss(gaussian.beta, data_random_2[i_min:i_max:, 0]), label='fit', color='C1')
ax[0].errorbar(data_random_2[i_min:i_max:, 0], data_random_2[i_min:i_max:, 1], label='data', linestyle = '', marker = ',', yerr = sigma_random_2_count[i_min:i_max:], xerr = sigma_random_2_time[i_min:i_max:])
ax[1].errorbar(data_random_2[i_min:i_max:, 0], data_random_2[i_min:i_max:, 1] - gauss(gaussian.beta, data_random_2[i_min:i_max:, 0]), yerr = sigma_random_2_count[i_min:i_max:], label='residuals', linestyle = '', marker = '.')
ax[1].hlines(0, data_random_2[i_min, 0], data_random_2[i_max, 0], color = 'C1')
ax[0].set(ylabel = '# Counts per second', title = 'Time difference spectrum fit (position: random 2)')
ax[1].set(xlabel = r'$\Delta t$ [ps]', ylabel = r'Data - Fit [$\frac{\#}{s}$]')
ax[0].text(0.015, 0.955, f'µ: {round(gaussian.beta[0], np.sqrt(gaussian.cov_beta[0, 0]))}\nsigma: {round(gaussian.beta[1], np.sqrt(gaussian.cov_beta[1, 1]))}\nchiq/dof: {np.around(chi_sq/dof, 2)}\np: {sigfigRound(float(p), sigfigs = 3)}', horizontalalignment='left', verticalalignment='top', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[0].grid()
ax[1].grid()
ax[0].legend()
ax[1].legend()
fig.tight_layout()
fig.savefig(f'./output/delta_t_random_2_fit.pdf')
plt.clf()

# delta time and delta x
delta_t = gaussian.beta[0]
sigma_delta_t = sigma_t
delta_x = scc.c * delta_t * 1e-10
sigma_delta_x = scc.c * sigma_delta_t * 1e-10
d_1 = (D + delta_x)/2
sigma_d_1 = 0.5 * np.sqrt((sigma_D)**2 + sigma_delta_x**2)
print(f'\n\t delta t: {round(delta_t, sigma_delta_t)} ps')
print(f'\t delta x: {round(delta_x, sigma_delta_x)} cm')
print(f'\t d_1: {round(d_1, sigma_d_1)} cm \n')


print('////////// position: random 3 //////////')
i_min = np.max(np.where(data_random_3[:, 0] < 200))
i_max = np.max(np.where(data_random_3[:, 0] < 900))
gaussian = gaussian_fit(data_random_3[i_min:i_max:, 0], data_random_3[i_min:i_max:, 1], sigma_random_3_time, sigma_random_3_count, my0 = 500 , sigma0 = 166, A0 = np.max(data_random_3[:, 1])*np.sqrt(2*np.pi * 166**2))
print(f'{gaussian.beta=}')
print(f'errors: {np.sqrt(gaussian.cov_beta[0, 0])}, {np.sqrt(gaussian.cov_beta[1, 1])}, {np.sqrt(gaussian.cov_beta[2, 2])}, {np.sqrt(gaussian.cov_beta[1, 1])}')
#print(f'{gaussian.res_var=}')
chi_sq = np.sum(((data_random_3[i_min:i_max:, 1] - gauss(gaussian.beta, data_random_3[i_min:i_max:, 0]))/sigma_random_3_count[i_min:i_max:])**2)
dof = i_max - i_min
print(f'chiq/dof: {chi_sq/dof}')
p = 1 - scs.chi2.cdf(chi_sq, dof)
print(f'{p=}')

fig, ax = plt.subplots(nrows = 2, sharex = True)
ax[0].plot(data_random_3[i_min:i_max:, 0], gauss(gaussian.beta, data_random_3[i_min:i_max:, 0]), label='fit', color='C1')
ax[0].errorbar(data_random_3[i_min:i_max:, 0], data_random_3[i_min:i_max:, 1], label='data', linestyle = '', marker = ',', yerr = sigma_random_3_count[i_min:i_max:], xerr = sigma_random_3_time[i_min:i_max:])
ax[1].errorbar(data_random_3[i_min:i_max:, 0], data_random_3[i_min:i_max:, 1] - gauss(gaussian.beta, data_random_3[i_min:i_max:, 0]), yerr = sigma_random_3_count[i_min:i_max:], label='residuals', linestyle = '', marker = '.')
ax[1].hlines(0, data_random_3[i_min, 0], data_random_3[i_max, 0], color = 'C1')
ax[0].set(ylabel = '# Counts per second', title = 'Time difference spectrum fit (position: random 3)')
ax[1].set(xlabel = r'$\Delta t$ [ps]', ylabel = r'Data - Fit [$\frac{\#}{s}$]')
ax[0].text(0.015, 0.955, f'µ: {round(gaussian.beta[0], np.sqrt(gaussian.cov_beta[0, 0]))}\nsigma: {round(gaussian.beta[1], np.sqrt(gaussian.cov_beta[1, 1]))}\nchiq/dof: {np.around(chi_sq/dof, 2)}\np: {sigfigRound(float(p), sigfigs = 3)}', horizontalalignment='left', verticalalignment='top', transform=ax[0].transAxes, bbox=dict(facecolor='white', edgecolor='gray'))
ax[0].grid()
ax[1].grid()
ax[0].legend()
ax[1].legend()
fig.tight_layout()
fig.savefig(f'./output/delta_t_random_3_fit.pdf')
plt.clf()

# delta time and delta x
delta_t = gaussian.beta[0]
sigma_delta_t = sigma_t
delta_x = scc.c * delta_t * 1e-10
sigma_delta_x = scc.c * sigma_delta_t * 1e-10
d_1 = (D + delta_x)/2
sigma_d_1 = 0.5 * np.sqrt((sigma_D)**2 + sigma_delta_x**2)
print(f'\n\t delta t: {round(delta_t, sigma_delta_t)} ps')
print(f'\t delta x: {round(delta_x, sigma_delta_x)} m ')
print(f'\t d_1: {round(d_1, sigma_d_1)} cm \n')
