import numpy as np

def histParse(filename):
    file = open(filename, 'r')
    raw = file.readlines()

    print(f'{filename} read')
    dt = raw[2].split('\t')[0]
    print(f'dt: {dt}')

    temp = raw[2].split('\t')[2].replace('; \n', '').replace(' ', '')
    temp = temp.split(';')

    data = np.zeros((len(temp), 2))
    for i in range(data.shape[0]):
        data[i, 0] = temp[i].split(',')[0]
        data[i, 1] = temp[i].split(',')[1]

    return data.astype('float')
