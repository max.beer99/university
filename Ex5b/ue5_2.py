from ROOT import TLorentzVector
import numpy as np
import scipy.constants as scc

# initialisiere TLorentzVector-Objekt und setze Pt, eta, phi und M. Damit ist der Vierervektor vollstaendig bestimmt
e_1 = TLorentzVector()
e_2 = TLorentzVector()
p_1 = TLorentzVector()
p_2 = TLorentzVector()

# set constants
m_e = 511e03 # mass of the electron
m_Z = 91.2e09 # mass of the Z-boson

e_1.SetPtEtaPhiM(14.94e09, -0.456, 0.466, m_e)
e_2.SetPtEtaPhiM(44.35e09, 0.186, 2.676, m_e)
p_1.SetPtEtaPhiM(14.25e09, 0.266, 0.207, m_e)
p_2.SetPtEtaPhiM(47.08e09, -0.422, -1.147, m_e)

# add all vectors
Z_1 = e_1 + p_1
Z_2 = e_2 + p_2
H = Z_1 + Z_2

# calculate the masses
m_H = H.M()
m_Z_1 = Z_1.M()
m_Z_2 = Z_2.M()

# print all

print("Literature for Z-boson Mass: m_Z = {} GeV".format(m_Z/1e09))
print("Z_1 Mass: m_Z_1 = {} GeV \t difference from m_Z: {} %".format(m_Z_1/1e09, np.abs(m_Z_1 - m_Z)/m_Z *1e02))
print("Z_2 Mass: m_Z_2 = {} GeV \t difference from m_Z: {} %".format(m_Z_2/1e09, np.abs(m_Z_2 - m_Z)/m_Z *1e02))
print("Higgs Mass: m_H = {} GeV".format(m_H/1e09))
